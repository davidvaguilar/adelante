@extends('layouts.panel')

@section('content')
<div class="card shadow">
  <div class="card-header border-0">
    <div class="row align-items-center">
      <div class="col">
        <h3 class="mb-0">Editar categoria</h3>
      </div>
      <div class="col text-right">
        <a href="{{ url('specialties')}}" class="btn btn-sm btn-default">
          Cancelar y volver
        </a>
      </div>
    </div>
  </div>

  <div class="card-body">
    @if( $errors->any() )
      <div class="alert alert-danger" role="alert">
        <ul>
          @foreach( $errors->all() as $error )
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <form action="{{ url('/specialties/'.$specialty->id) }}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}
      {{ method_field('PUT') }}
      <div class="form-group">
        <label for="name">Nombre de la categoria</label>
        <input type="text" name="name" class="form-control" value="{{ old('name', $specialty->name) }}">
      </div>
      <div class="form-group">
        <label for="description">Descripción</label>
        <input type="text" name="description" class="form-control" value="{{ old('description', $specialty->description) }}">
      </div>
      <div class="form-group">
        <label for="photo">Imagen</label>
        <input type="file" name="file" class="form-control" value="{{ old('photo') }}" accept="image/*">
        
        <div class="text-center">
          <img src="{{ asset($specialty->url) }}"  height="120px"  />
        </div>
      </div>
      <button type="submit" class="btn btn-primary"> Guardar cambios </button>
    </form>

  </div>
</div>

@endsection
