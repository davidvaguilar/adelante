@extends('layouts.panel')

@section('content')
<div class="card shadow">
  <div class="card-header border-0">
    <div class="row align-items-center">
      <div class="col">
        <h3 class="mb-0">Registrar nuevas reserva</h3>
      </div>
      <div class="col text-right">
        <a href="{{ url('patients')}}" class="btn btn-sm btn-default">
          Cancelar y volver
        </a>
      </div>
    </div>
  </div>

  <div class="card-body">
    @if( $errors->any() )
      <div class="alert alert-danger" role="alert">
        <ul>
          @foreach( $errors->all() as $error )
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <form action="{{ url('appointments') }}" method="POST">
      {{ csrf_field() }}
     <!-- <div class="form-group">
        <label for="description">Descripción</label>
        <input name="description" id="description" value="{{-- old('description') --}}" type="text" class="form-control" placeholder="Describe brevemente la consulta" required>
      </div> -->

      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="specialty">Categoria</label>
          <select name="specialty_id" id="specialty" class="form-control" required>
            <option value="">Seleccionar especialidad</option>
            @foreach( $specialties as $specialty )
              <option value="{{ $specialty->id }}" @if( old('specialty_id') == $specialty->id ) selected @endif>{{ $specialty->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group col-md-6">
          <label for="doctor">Sucursal</label>
          <select name="doctor_id" id="doctor" class="form-control" required>

            @foreach( $doctors as $doctor )
              <option value="{{ $doctor->id }}" @if( old('doctor_id') == $doctor->id ) selected @endif>{{ $doctor->name }}</option>
            @endforeach

          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="type">Tipo de atención</label>
        <select name="type_id" id="type" class="form-control" required>
          @foreach( $types as $type )
            <option value="{{ $type->id }}" @if( old('type_id') == $type->id ) selected @endif>{{ $type->name }}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label for="dni">Fecha</label>
        <div class="input-group input-group-alternative">
          <div class="input-group-prepend">
             <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
          </div>
          <input class="form-control datepicker" placeholder="Seleccionar fecha" type="text"
                  id="date" name="scheduled_date"
                  value="{{ old('scheduled_date', date('Y-m-d')) }}"
                  data-date-format="yyyy-mm-dd"
                  data-date-start-date="{{ date('Y-m-d') }}"
                  data-date-end-date="+30d">
        </div>
      </div>

      <div class="form-group">
        <label for="address">Hora de atencion</label>
        <div id="hours">
          @if( $intervals )
            @foreach( $intervals['morning'] as $key => $interval )
              <div class="custom-control custom-radio mb-3">
                <input name="scheduled_time" value="{{ $interval['start'] }}" class="custom-control-input" id="intervalMorning{{ $key }}" type="radio" required>
                <label class="custom-control-label" for="intervalMorning{{ $key }}">{{ $interval['start'] }} - {{ $interval['end'] }}</label>
              </div>
            @endforeach
            @foreach( $intervals['afternoon'] as $key => $interval )
              <div class="custom-control custom-radio mb-3">
                <input name="scheduled_time" value="{{ $interval['start'] }}" class="custom-control-input" id="intervalMorning{{ $key }}" type="radio" required>
                <label class="custom-control-label" for="intervalAfternoon{{ $key }}">{{ $interval['start'] }} - {{ $interval['end'] }}</label>
              </div>
            @endforeach
          @else
            <div class="alert alert-info" role="alert">
              Seleccion un medio y una fecha, para ver sus horas disponibles.
            </div>
          @endif
        </div>
      </div>
    
      <button type="submit" class="btn btn-primary"> Guardar </button>
    </form>
  </div>
</div>

@endsection

@section('scripts')
  <script src="{{ asset('/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script>
    let $doctor, $type, $date, $specialty, $hours;
    let iRadio;
    const noHoursAlert = `<div class="alert alert-danger" role="alert">
        <strong>Lo sentimos!</strong> No se encontraron horas disponibles para el medico en el dia seleccionado.
      </div>`;

    $(function() {
      $specialty = $('#specialty');
      $doctor = $('#doctor');
      $type = $('#type');
      $date = $('#date');
      $hours = $('#hours');

      $specialty.change(() => {
        const specialtyId = $specialty.val();
        const url =  `/api/specialties/${specialtyId}/doctors`;
        $.getJSON(url, onDoctorsLoaded);
      });



      $doctor.change(() => {
        const doctorId = $doctor.val();
        const url =  `/api/users/${doctorId}/types`;
        $.getJSON(url, onTypesLoaded);
      });



      $type.change(loadHours);
      $date.change(loadHours);
    });

    function onDoctorsLoaded(doctors){
   
      let htmlOptions = '';
      let bandera = true;
      doctors.forEach(doctor => {
        htmlOptions += `<option value="${doctor.id}">${doctor.name}</option>`;
        if( bandera ){
          const url =  `/api/users/${doctor.id}/types`;
          $.getJSON(url, onTypesLoaded);
        }
        bandera = false;
      });
      $doctor.html(htmlOptions);
      if( bandera ){
        $type.html('');
      }
    }


    function onTypesLoaded(types){
      let htmlOptions = '';
      types.forEach(type => {
        htmlOptions += `<option value="${type.id}">${type.name}</option>`;
      });
      $type.html(htmlOptions);
      loadHours();  //side-effect
    }


    function loadHours(){
      const selectedDate = $date.val();
      const doctorId = $doctor.val();
      const typeId = $type.val();
      const url = `/api/schedule/hours?date=${selectedDate}&doctor_id=${doctorId}&type_id=${typeId}`;
      $.getJSON(url, displayHours);
    }

    function displayHours(data){
      if( !data.morning && !data.afternoon || data.morning.length === 0 && data.afternoon.length === 0 ){
        $hours.html(noHoursAlert);
        return;
      }
      let htmlHours = '';
      iRadio = 0;
      if( data.morning ){
        const morning_intervals = data.morning;
        morning_intervals.forEach(interval => {
          htmlHours += getRadioIntervalHtml(interval);
          //console.log(`${interval.start} - ${interval.end}`);
        });
      }
      if( data.afternoon ){
        const afternoon_intervals = data.afternoon;
        afternoon_intervals.forEach(interval => {
          htmlHours += getRadioIntervalHtml(interval);
          //console.log(`${interval.start} - ${interval.end}`);
        });
      }
      $hours.html(htmlHours);
    }

    function getRadioIntervalHtml(interval) {
      const text = `${interval.start} - ${interval.end}`;
      return `<div class="custom-control custom-radio mb-3">
          <input name="scheduled_time" value="${interval.start}" class="custom-control-input" id="interval${iRadio}" type="radio">
          <label class="custom-control-label" for="interval${iRadio++}">${text}</label>
        </div>`;
    }

  </script>
@endsection
