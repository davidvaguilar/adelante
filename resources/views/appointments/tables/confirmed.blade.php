<div class="table-responsive " style="display: inline-flex;">
  <table id="example1" class="table align-items-center table-flush" >
    <thead class="thead-light">
      <tr>
        <th scope="col">Id Reserva</th>
        <!-- <th scope="col">Descripción</th>  -->
        <th scope="col" style="min-width: 100px;">Categoria</th>
        @if( $role == 'patient' )
          <th scope="col" style="min-width: 100px;">Empresa</th>
        @elseif( $role == 'doctor' )
          <th scope="col" style="min-width: 100px;">Cliente</th>
          <th scope="col" style="min-width: 100px;">Telefono</th>
        @endif
        <th scope="col" style="min-width: 100px;">Fecha</th>
        <th scope="col" style="min-width: 100px;">Hora</th>
        <th scope="col" style="min-width: 100px;">Tipo</th>
        <th scope="col" style="min-width: 100px;">Opciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach( $confirmedAppointments as $appointment )
      <tr>
        <th scope="row">
          {{ $appointment->id }}
        </th>
        <!--<th scope="row">
          {{-- $appointment->description --}}
        </th>-->
        <td>
          {{ $appointment->specialty->name }}
        </td>
        @if( $role == 'patient' )
          <td>
            {{ $appointment->doctor->name }}
          </td>
        @elseif( $role == 'doctor' )
          <td>
            {{ $appointment->patient->name }}
          </td>
          <td>
            {{ $appointment->patient->phone }}
          </td>
        @endif
        <td>
          {{ $appointment->scheduled_date->format('d-m-Y') }}
        </td>
        <td>
          {{ $appointment->scheduled_time_12 }}
        </td>
        <td>
          {{ $appointment->type->name }}
        </td>
        <td>
          @if( $role== 'admin' )
            <a href="{{ url('/appointments/'.$appointment->id) }}" class="btn btn-sm btn-primary" title="Ver cita">
              Ver
            </a>
          @endif
          @if( $role == 'doctor' || $role == 'admin' )
            <form action="{{ url('/appointments/'.$appointment->id.'/attended') }}" method="POST" class="d-inline-block">
              {{ csrf_field() }}
              <button type="submit" class="btn btn-sm btn-success" data-toggle="tooltip" title="Confirmar atención">
                <i class="ni ni-check-bold"></i>
              </button>
            </form>
          @endif
          <a href="{{ url('/appointments/'.$appointment->id.'/cancel') }}" class="btn btn-sm btn-danger" title="Cancelar cita">
            Cancelar
          </a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

<div class="card-footer py-4">
  {{-- $confirmedAppointments->links() --}}
</div>
