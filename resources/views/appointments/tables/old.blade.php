<div class="table-responsive">
  <table class="table align-items-center table-flush">
    <thead class="thead-light">
      <tr>
        <th scope="col">Id Reserva</th>
        <th scope="col">Categoria</th>
        @if( $role == 'patient' )
          <th scope="col" style="min-width: 100px;">Empresa</th>
        @elseif( $role == 'doctor' )
          <th scope="col" style="min-width: 100px;">Cliente</th>
          <th scope="col" style="min-width: 100px;">Telefono</th>
        @endif
        <th scope="col">Fecha</th>
        <th scope="col">Hora</th>
        <th scope="col">Estado</th>
        <th scope="col">Opciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach( $oldAppointments as $appointment )
      <tr>
        <th scope="row">
          {{ $appointment->id }}
        </th>
        <th scope="row">
          {{ $appointment->specialty->name }}
        </th>
        @if( $role == 'patient' )
          <td>
            {{ $appointment->doctor->name }}
          </td>
        @elseif( $role == 'doctor' )
          <td>
            {{ $appointment->patient->name }}
          </td>
          <td>
            {{ $appointment->patient->phone }}
          </td>
        @endif
        <td>
          {{ $appointment->scheduled_date->format('d-m-Y') }}
        </td>
        <td>
          {{ $appointment->scheduled_time_12 }}
        </td>
        <td>
          {{ $appointment->status }}
        </td>
        <td>
          <a href="{{ url('/appointments/'.$appointment->id) }}" class="btn btn-primary btn-sm">
            Ver
          </a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>


<div class="card-footer py-4">
  {{-- $oldAppointments->links() --}}
</div>
