<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="description" content="">
    <script src="{{ asset('landing/themekit/scripts/jquery.min.js') }}"></script>
    <script src="{{ asset('landing/themekit/scripts/main.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/bootstrap-grid.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/glide.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/content-box.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/contact-form.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/media-box.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/skin.css') }}">
    <link rel="icon" href="{{ asset('img/adelante-ico.jpeg') }}">

    <link href="{{ asset('argon-dashboard/assets/vendor/select2/dist/css/select2.min.css') }}" rel="stylesheet">
</head>

<body>
    <div id="preloader"></div>
    <nav class="menu-classic menu-transparent menu-fixed menu-one-page align-right light" data-menu-anima="fade-bottom" data-scroll-detect="true">
        <div class="container">
            <div class="menu-brand"  style="height: 80px;">
                <a href="#">
                    <img class="logo-default" src="{{ asset('img/items/logo_nav.png') }}" alt="logo" />
                    <img class="logo-retina" src="{{ asset('img/items/logo_nav.png') }}" alt="logo" />
                </a>
            </div>
            <i class="menu-btn"></i>
            <div class="menu-cnt">
                <ul>
                    <li>
                        <a href="{{ url('/') }}">
                          <img src="{{ asset('landing/media/home_icon.png') }}" alt="" style="max-height: 30px; margin-top: 10px;">
                        </a>
                    </li>
                    <li>
                        <a href="#about">¿POR QUÉ ADELANTE.CL?</a>
                    </li>
                   
                    @guest
                    <li>
                        <a href="{{ url('register') }}">REGISTRATE</a>
                    </li>
                    <li>
                        <a href="{{ route('login') }}">INICIAR SESIÓN</a>
                    </li>
                    @else
                    <li>
                        <a href="{{ url('profile') }}" style="padding: 0px 0px 0px 30px;" >
                            @if( Auth::user()->avatar )
                                <img src="{{ Auth::user()->avatar }}" alt="Imagen de perfil" height="28">
                            @else
                              <img src="{{ asset('img/adelante-ico.jpeg') }}" alt="Imagen de perfil" style="max-height: 35px; margin-top: 10px;">
                            @endif
                        </a>
                    </li>

                    <li>
                        <a   href="{{ url('profile') }}" style="padding: 0px 0px 0px 0px;" >
                            {{ Auth::user()->name }} 
                        </a>
                    </li>
                    @endguest
                </ul>
                <div class="menu-right">
                    <div class="menu-custom-area">
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </nav>
    <main>
        <section class="section-image section-full-width-right light no-padding-top section-bottom-layer" 
              style="background-color: rgb(41, 41, 124);background-image:url({{ asset('landing/media/fondo.png') }});">
           
            <div class="container">
                <div class="row">
                    <div class="col-lg-12" data-anima="fade-in" data-time="1000">
                        <hr class="space" />
                        <hr class="space-lg" />
                        <h2 style="text-align: center;">¿QUÉ TRÁMITE DESEAS REALIZAR?</h2>
                        <!--  class="mt-4 mb-3" <input class="input-text" type="text" placeholder="" style="width: 50%;margin-left: 25%;margin-right: 25%;"> -->
                        <form style="margin-left: 15%;margin-right:15%;" action="{{ url('/attention') }}" method="GET">
                            <select id="job_create_branch" name="branch_id"  onchange="this.form.submit()"
                                        class="input-text select2" style="width: 100%;margin-left: 15%;margin-right: 15%;">
                                <option value="">Busca una sucursal</option>
                                @foreach ($branches as $branch)
                                    <option value="{{ $branch->id }}">
                                    {{ $branch->name }}</option>
                                @endforeach
                          </select>
                        </form>
                        <br><br><br>
                     
                        <table class="table table-grid table-md-6">
                          <tbody>
                            <tr>
                              @foreach($categories as $key => $category)  
                                @if( $category->url )
                                  <td style="text-align: center;">
                                  
                                    <a href="{{ url('/categories/'.$category->id) }}">
                                      <img src="{{ url($category->url) }}" width="95px" height="110px" style="border-radius: 10%;"/>
                                      {{-- $category->name --}}
                                    </a>
                                    
                                  </td>
                                  @if( ($key % 2) == 1 )
                                      </tr>
                                  </tbody>
                                  <tbody>
                                      <tr>
                                  @endif

                                @endif
                              @endforeach

                            </tr>
                          </tbody>
                        </table><br><br><br><br>
                        <table class="table table-grid table-md-6">
                            <tbody>
                                <tr>
                                    <td style="text-align: center;">
                                        <img src="{{ asset('img/items/ico_01.png') }}" alt="" style="width:100px;">
                                        <h2>MÁS CONTROL</h2>
                                        <h4>Con Adelanteya.cl observa y
                                            <h4>ten un mejor control del
                                                <h4>acceso y trazabilidad de tus
                                                    <h4>clientes en tus negocios
                                    </td>
                                    <td style="text-align: center;">
                                        <img src="{{ asset('img/items/ico_02.png') }}" alt="" style="width:100px;">
                                        <h2>MÁS SIMPLE Y COMPLETO</h2>
                                        <h4>Plataforma amigable e
                                            <h4>intuitiva, todo lo que
                                                <h4>necesitas en un solo lugar
                                    </td>
                                    <td style="text-align: center;">
                                        <img src="{{ asset('img/items/ico_03.png') }}" alt="" style="width:100px;">
                                        <h2>AHORRA TIEMPO</h2>
                                        <h4>Ordena y dale mejor
                                            <h4>eficiencia a tu negocio, no
                                                <h4>gastes mas tiempo en tareas
                                                    <h4>que adelanteya.cl hará por ti..
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        </section>
        <section class="section-base">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-6" data-anima="fade-left" data-time="1000">
                        <td>
                            <img src="{{ asset('img/items/ico_04.png') }}" alt="" style="margin-left: 25%;">
                        </td>
                        <h2 class="themekit-header">SISTEMA DE AGENDAMIENTO ONLINE</h2>
                        <p>
                            Simplifica de trabajo de toma de horas y control de horarios. Con Adelante.cl podrás acceder a tu agenda y gestionar tus citas desde cualquier dispositivo móvil y lugar.
                        </p>
                    </div>
                    <div class="col-lg-6 col-md-6 align-right" data-anima="fade-right" data-time="1000">
                        <img class="margin-23" src="{{ asset('img/items/img_01.png') }}" alt="">
                    </div>
                </div>
            </div>
        </section>
        <section id="about" class="section-image" 
            style="background-image:url({{ asset('landing/media/fondo.png') }});background-color: rgb(41, 41, 124)"><br>
            <h1 style="margin-left:30%;margin-top: 20px;color: white;">¿POR QUÉ ADELANTE.CL?</h1>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-6" data-anima="fade-left" data-time="1000">
                        <img class="margin-23" src="{{ asset('img/items/img_02.png') }}" alt="">
                    </div>
                    <div class="col-lg-6 col-md-6" data-anima="fade-right" data-time="1000">
                        <td>
                            <img src="{{ asset('img/items/ico_05.png') }}" alt="" style="margin-left: 25%;">
                        </td>
                        <h2 class="themekit-header" style="color: white;">FICHA ELECTRÓNICA</h2>
                        <p style="color: white;">
                            Administra el historial de atenciones de tus clientes y sus lleva un control diario de visitas y atenciones.    
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="section-base">
            <div class="container">
                <div class="row align-items-center" >
                    <div class="col-lg-6 col-md-6" data-anima="fade-left" data-time="1000">
                        <td>
                            <img src="{{ asset('img/items/ico_06.png') }}" alt="" style="margin-left: 25%;">
                        </td>
                        <td class="align-center">
                            <h2  style="color: #5596D6;" class="themekit-header">REPORTE Y ESTADISTICA</h2>
                            <p>
                                Todo lo que necesitas saber a solo un clic. Observa de manera rápida y simple el rendimiento diario de tu negocio.
                            </p>
                        </td>
                    </div>
                    <div class="col-lg-6 col-md-6 align-right" data-anima="fade-right" data-time="1000">
                        <img class="margin-23" src="{{ asset('img/items/img_03.png') }}" alt="">
                    </div>
                </div>
            </div>
        </section>
    </main>
    <i class="scroll-top-btn scroll-top show"></i>
    <footer class="light">
        <section class="section-image light align-center " 
            style="background-color: rgb(56, 117, 167);background-image:url({{ asset('landing/media/fondo.png') }}); ">
            <div class="container" style="padding: 40px;">
                <img src="{{ asset('img/items/logo_nav.png') }}"  style="height: 70px;" alt="">
                <div class="col-lg-4" style="text-align: center; margin-left: 25%;">
                    <div class="icon-links icon-social social-colors align-right align-left-md">
                        <a class="facebook"><img src="{{ asset('landing/media/fb.png') }}" alt=""></a>
                        <a class="twitter"><img src="{{ asset('landing/media/ig.png') }}" alt=""></a>
                        <a class="linkedin"><img src="{{ asset('landing/media/tw.png') }}" alt=""></a>
                        <a class="pinterest"><img src="{{ asset('landing/media/yt.png') }}" alt=""></a>
                    </div>
                </div>
            </div>
        </section>
    </footer>
        
    
    <section class="section-base">
        <div class="container" style="padding: 20px;">
            <div class="row align-items-center">
            
                <table class="table table-grid table-md-6" style="color: #5596D6; text-align: left;">
                    <tbody>
                        <tr>
                            <td>
                                <h2 style="color: #5596D6;">COMERCIAL</h2>
                                Productos<br>
                                Contacta con un vendedor
                            </td>
                            <td>
                                <h2 style="color: #5596D6;">SOPORTE</h2>
                                +562 26665563<br>
                                soporte@adelanteya.cl
                            </td>
                            <td></td>
                            <td>
                                <img src="{{ asset('img/items/corfo.png') }}"   alt="">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </section>
      
    <footer class="light">
        <section class="section-image light align-center" 
            style="background-color: rgb(56, 117, 167);background-image:url({{ asset('landing/media/fondo.png') }}); ">
            <div class="container" style="padding: 20px;">

                <p>Todos los derechos reservados ©2021 Adelanteya.cl </p>
                <br>
            </div>
        </section>
      
        <link rel="stylesheet " href="{{ asset('landing/themekit/media/icons/iconsmind/line-icons.min.css') }} ">
        <script src="{{ asset('landing/themekit/scripts/parallax.min.js') }} "></script>
        <script src="{{ asset('landing/themekit/scripts/glide.min.js') }} "></script>
        <script src="{{ asset('landing/themekit/scripts/magnific-popup.min.js') }} "></script>
        <script src="{{ asset('landing/themekit/scripts/tab-accordion.js') }} "></script>
        <script src="{{ asset('landing/themekit/scripts/imagesloaded.min.js') }} "></script>
        <script src="{{ asset('landing/themekit/scripts/progress.js') }} "></script>
        <script src="{{ asset('landing/media/custom.js') }} "></script>
        
        <script src="{{ asset('argon-dashboard/assets/vendor/select2/dist/js/select2.full.min.js') }}"></script>
    </footer>

    <script>

      $('.select2').select2();

    </script>
</body>

</html>