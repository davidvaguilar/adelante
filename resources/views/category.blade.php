
@extends('layouts.main')

@section('content')

    <main>
      <section class="section-image section-full-width-right light no-padding-top"  
            style="background-color: rgb(41, 41, 124);background-image:url({{ asset('landing/media/fondo.png') }});"> <!--  section-bottom-layer -->
        <div class="container">
              <div class="row">
                  <div class="col-lg-12" data-anima="fade-in" data-time="1000">
                      <hr class="space" />
                      <hr class="space-lg" />
                      <h2 style="text-align: center;">¿QUÉ TRÁMITE DESEA REALIZAR?</h2>
                      <!--  class="mt-4 mb-3" <input class="input-text" type="text" placeholder="" style="width: 50%;margin-left: 25%;margin-right: 25%;"> -->
                      <form style="margin-left: 15%;margin-right:15%;" action="{{ url('/attention') }}" method="GET">
                          <select id="job_create_branch" name="branch_id"  onchange="this.form.submit()"
                                      class="input-text select2" style="width: 100%;margin-left: 15%;margin-right: 15%;">
                              <option value="">Busca una sucursal</option>
                              @foreach ($branches as $branch)
                                  <option value="{{ $branch->id }}">
                                  {{ $branch->name }}</option>
                              @endforeach
                        </select>
                      </form>
                      <br><br><br>
                    
                      <table class="table table-grid table-md-6">
                        <tbody>
                          <tr>
                            @foreach($category->users as $key => $user)  
                              @if( $user->url )
                                <td style="text-align: center;">
                                
                                  <a href="{{ url('/attention/'.$user->id) }}">
                                    <img src="{{ url($user->url) }}"  style="border-radius: 2%;"/><br>
                                    {{-- $user->name --}}
                                  </a>
                                  
                                </td>
                                @if( ($key % 2) == 1 )
                                    </tr>
                                </tbody>
                                <tbody>
                                    <tr>
                                @endif

                              @endif
                            @endforeach

                          </tr>
                        </tbody>
                      </table><br><br><br><br>
              
                  </div>
              </div>
      </section>
     
    </main>
@endsection
