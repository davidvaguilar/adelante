<body>
    <h5>Número de reserva: {{ $created->id }}</h5>

    <span>Hora de reserva: {{ $created->scheduled_date->format('d-m-Y') }} {{ $created->scheduled_time_12 }}</span>

    <p>
        <span> Tu reserva ya fue confirmada </span>
    </p>
</body>