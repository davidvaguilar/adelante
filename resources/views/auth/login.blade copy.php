@extends('layouts.form')

@section('title', 'Inicio de sesion')
@section('title', 'Ingresa tus datos para iniciar sesion')

@section('content')
<div class="container mt--8 pb-5">
  <div class="row justify-content-center">
    <div class="col-lg-5 col-md-7">
      <div class="card bg-secondary shadow border-0">


        <div class="card-header bg-transparent">
          <div class="btn-wrapper text-center">
          <a href="{{ url('login/google') }}" class="btn btn-neutral btn-icon">
                <span class="btn-inner--icon"><img src="{{ asset('img/icons/common/google.svg') }}"></span>
                <span class="btn-inner--text">Google</span>
              </a>
              <a href="{{ url('login/facebook') }}" class="btn btn-neutral btn-icon">
                <span class="btn-inner--icon"><img src="{{ asset('img/icons/common/facebook2013.png') }}"></span>
                <span class="btn-inner--text">Facebook</span>
              </a>
          </div>
        </div>


        <div class="card-body px-lg-5 py-lg-5">
          @if( $errors->any() )
            <div class="alert alert-danger" role="alert">
              <strong>Error!</strong> {{ $errors->first() }}
            </div>
          @endif

       

          <form role="form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="form-group mb-3">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                </div>
                <input class="form-control" placeholder="Email" type="email" name="email" value="{{ old('email') }}" required autofocus>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                </div>
                <input class="form-control" placeholder="Password" type="password" name="password" required >
              </div>
            </div>
            <div class="custom-control custom-control-alternative custom-checkbox">
              <input name="remember" class="custom-control-input" id=" remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
              <label class="custom-control-label" for="remember">
                <span class="text-muted">Recordar sesión</span>
              </label>
            </div>
            <div class="text-center">
              <button type="submit" class="btn btn-primary my-4">Ingresar</button>
            </div>
          </form>
        </div>
      </div>
   
    </div>
  </div>
</div>
@endsection
