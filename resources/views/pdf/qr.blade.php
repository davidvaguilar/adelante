<!DOCTYPE html>
<html lang="es">
<head>
  <title>{{ config('app.name', 'Laravel') }}</title>
  <meta charset="utf-8">
</head>
<style>
html {
	margin: 0pt 0pt;
}
</style>

<body style="background-image:url({{ asset('img/formato_qr.jpg') }}); background-repeat: no-repeat;">

  <div style="line-height:400px; margin:340px auto; text-align:center;">

    <img src="data:image/png;base64, {!! $qrcode !!}"/>
  
  </div>

</body>
</html>
