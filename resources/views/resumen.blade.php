

@extends('layouts.main2')

@section('content')

    <!-- Header -->
    <div class="header pb-5 pt-4 pt-md-4">  <!--   pb-8 pt-4 pt-md-6-->

    </div>
    <!-- Page content -->
    <div class="container-fluid">   <!--   mt--7 -->
     

      <h3 class="navbar-heading text-uppercase">
        <strong>INICIO / {{ $type->user->specialties->first()->name }} / {{ $type->user->name }} /  {{ $type->type->name }} / <span class="text-primary">RESERVA</span> </strong>
      </h3>
             <br>


      <div class="card shadow">
       
        <div class="card-body">

          <div class="row">
            <div class="col text-center">
              <i class="fa fa-check-circle fa-4x text-success"></i>
              <h2 class="card-title mb-0"><strong>¡Reserva realizada!</strong> {{-- $created->id --}}</h2><br>
              <span class="text-center text-muted mb-0">Revisa tu correo electronico y no olvides presentar tu {{-- $created->scheduled_date->format('d-m-Y') --}} {{-- $created->scheduled_time_12 --}}</span><br>
              <span class="text-center text-muted mb-0">comprobante el dia y hora de tu reserva.</span><br><br>
              
              <strong><span class="text-center">Recuerda llegar 10 minutos antes.</span></strong>
            </div>
             
          </div>
           <!--<p class="mt-3 mb-0 text-muted text-sm">
            <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> Tu reserva ya fue confirmada </span>
           <span class="text-nowrap">Hora de reserva: {{-- $created->scheduled_date --}} {{-- $created->scheduled_time --}}</span>
          </p>-->
    
        </div>
        <div class="card-footer text-center">
            <a type="button" href="{{ url('/workday/'.$type->id) }}" class="btn btn-default"> Volver Atrás </a>
        </div>
      </div>
   
      
      <!-- Footer -->
     {{-- @include('includes.panel.footer')  --}}
    </div>
    

@endsection


@section('scripts')
  <script>
    document.getElementById("btn-workday-active").innerHTML="ACTIVO";
    document.getElementById("btn-workday-active").classList.add('btn-success');
  </script>
@endsection
