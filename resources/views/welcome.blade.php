@extends('layouts.main')

@section('content')
  <main>
      <section class="section-image section-full-width-right light no-padding-top section-bottom-layer" 
            style="background-color: rgb(55,59,195); background-image:url({{ asset('landing/media/fondo.png') }});">
          
          <div class="container">
              <div class="row">
                  <div class="col-lg-12" data-anima="fade-in" data-time="1000">
                      <hr class="space" />
                      <hr class="space-lg" />
                      <h3 style="text-align: center;">¿QUÉ TRÁMITE DESEAS REALIZAR?</h3>
                      <!--  class="mt-4 mb-3" <input class="input-text" type="text" placeholder="" style="width: 50%;margin-left: 25%;margin-right: 25%;"> -->
                      <form style="margin-left: 15%;margin-right:15%;" action="{{ url('/attention') }}" method="GET">
                          <select id="job_create_branch" name="branch_id"  onchange="this.form.submit()"
                                      class="input-text select2" style="width: 100%;margin-left: 15%;margin-right: 15%;">
                              <option value="">Busca una sucursal</option>
                              @foreach ($branches as $branch)
                                  <option value="{{ $branch->id }}">
                                  {{ $branch->name }}</option>
                              @endforeach
                        </select>
                      </form>
                      <br><br><br>
                    
                      <table class="table table-grid table-md-6">
                        <tbody>
                          <tr>
                            @foreach($branches as $key => $user)  

                         
                              

                              @if( $user->url )
                                <td style="text-align: center;">
                                
                                  <a href="{{ url('/attention/'.$user->id) }}">
                                    <img src="{{ url($user->url) }}" height="200px" style="border-radius: 10%;"/>
                                  </a>
                                  
                                </td>
                                @if( ($key % 2) == 1 )
                                    </tr>
                                </tbody>
                                <tbody>
                                    <tr>
                                @endif

                              @endif
                            @endforeach

                          </tr>
                        </tbody>
                      </table><br><br><br><br>
                      <table class="table table-grid table-md-6">
                          <tbody>
                              <tr>
                                  <td style="text-align: center;">
                                      <img src="{{ asset('img/items/ico_01.png') }}" alt="" style="width:100px;">
                                      <h3><strong>MÁS CONTROL</strong></h3>
                                      <span>Con Adelanteya.cl observa y
                                          ten un mejor control del
                                              acceso y trazabilidad de tus
                                                  clientes en tus negocios</span>
                                  </td>
                                  <td style="text-align: center;">
                                      <img src="{{ asset('img/items/ico_02.png') }}" alt="" style="width:100px;">
                                      <h3><strong>MÁS SIMPLE <br>Y COMPLETO</strong></h3>
                                      <span>Plataforma amigable e
                                         intuitiva, todo lo que
                                             necesitas en un solo lugar</span>
                                  </td>
                                  <td style="text-align: center;">
                                      <img src="{{ asset('img/items/ico_03.png') }}" alt="" style="width:100px;">
                                      <h3><strong>AHORRA TIEMPO</strong></h3>
                                      <span>Ordena y dale mejor
                                          eficiencia a tu negocio, no
                                              gastes mas tiempo en tareas
                                                  que adelanteya.cl hará por ti..</span>
                                  </td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
    
              </div>
      </section>
      <section class="section-base">
          <div class="container">
              <div class="row align-items-center">
                  <div class="col-md-6" style="text-align: center;" data-anima="fade-left" data-time="1000">
                      <td>
                          <img src="{{ asset('img/items/ico_04.png') }}" alt=""  style="width:150px;">
                     
                      <h3  style="color: #5797d7;"><strong>SISTEMA DE AGENDAMIENTO ONLINE</strong></h3>
                      <p>
                          Simplifica de trabajo de toma de horas y control de horarios. Con Adelante.cl podrás acceder a tu agenda y gestionar tus citas desde cualquier dispositivo móvil y lugar.
                      </p> </td>
                  </div>
                  <div class="col-lg-6 col-md-6 align-right" data-anima="fade-right" data-time="1000">
                      <img class="margin-23" src="{{ asset('img/items/img_01.png') }}" alt="">
                  </div>
              </div>
          </div>
      </section>
      <section id="about" class="section-image " 
          style="background-image:url({{ asset('landing/media/fondo.png') }});background-color: rgb(67,95,204)"><br>
          <h2  style="margin-top: 20px;color: white; text-align: center;"><strong>¿POR QUÉ ADELANTEYA.CL?</strong></h2>
          <div class="container"   >
              <div class="row">
                  <div class="col-md-6"  data-anima="fade-left" data-time="1000">
                      <img src="{{ asset('img/items/img_02.png') }}" alt="">
                  </div>
                  <div class="col-md-6" style="text-align: center;" data-anima="fade-right" data-time="1000">
                      <td>
                          <img src="{{ asset('img/items/ico_05.png') }}" alt=""  style="width:150px;">
                        <h3 style="color: white;"><strong>FICHA ELECTRÓNICA</strong></h3>
                        <p style="color: white;">
                            Administra el historial de atenciones de tus clientes y sus lleva un control diario de visitas y atenciones.    
                        </p>
                      </td>
                  </div>
              </div>
          </div>
      </section>
      <section class="section-base text-center">
          <div class="container text-center">
              <div class="row text-center " >
                  <div class="col-md-6" style="text-align: center;" data-anima="fade-left" data-time="1000">
                      <td>
                          <img src="{{ asset('img/items/ico_06.png') }}" alt=""   style="width:150px;" >
                      </td>
                      <td class="text-center">
                          <h3  style="color: #5797d7;"><strong>REPORTE Y ESTADISTICA</strong></h3>
                          <p>
                              Todo lo que necesitas saber a solo un clic. Observa de manera rápida y simple el rendimiento diario de tu negocio.
                          </p>
                      </td>
                  </div>
                  <div class="col-md-6 align-right" data-anima="fade-right" data-time="1000">
                      <img src="{{ asset('img/items/img_03.png') }}" alt="">
                  </div>
              </div>
          </div>
      </section>
  </main>
   
@endsection
