

@extends('layouts.main2')

@section('content')

    <!-- Header -->
    <div class="header pb-5 pt-4 pt-md-4">

    </div>
    <!-- Page content -->
    <div class="container ">

      <h3 class="navbar-heading text-uppercase">
        <strong>INICIO / {{ $type->user->specialties->first()->name }} / {{ $type->user->name }} /  {{ $type->type->name }} / <span class="text-primary">RESERVA</span> </strong>
      </h3>
                
      <div class="row justify-content-md-center">
      
        <form id="reservar_crear" action="{{ url('appointments') }}" method="POST">
          {{ csrf_field() }}
        
          <div class="col-lg-12 mb-lg-auto">
            
            <br>
            <div class="form-group">
              <h3 class="navbar-heading text-uppercase text-primary"><strong>RESERVA</strong></h3>
            </div>


            <input class="form-control" type="hidden" id="specialty" name="specialty_id"
                value="{{ $type->user->specialties->first()->id }}">

            <input class="form-control" type="hidden" id="doctor" name="doctor_id"
                value="{{ $type->user->id }}">

            <input class="form-control" type="hidden" id="type" name="type_id"
                value="{{ $type->type_id }}">
          
            <div class="form-group">
              <label for="dni">Fecha</label>
              <div class="input-group input-group-alternative">
                <!--<div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                </div>-->
                <input class="form-control" placeholder="Seleccionar fecha" type="date"
                        id="date" name="scheduled_date"
                        value="{{ old('scheduled_date', date('Y-m-d')) }}">
              </div>
            </div>

      
            <div class="form-group">
              <label for="address">Hora de atencion</label>
              <div id="hours">
                @if( $intervals )
                  @foreach( $intervals['morning'] as $key => $interval )
                    <div class="custom-control custom-radio mb-3">
                      <input name="scheduled_time" value="{{ $interval['start'] }}" class="custom-control-input" id="intervalMorning{{ $key }}" type="radio" required>
                      <label class="custom-control-label" for="intervalMorning{{ $key }}">{{ $interval['start'] }} - {{ $interval['end'] }}</label>
                    </div>
                  @endforeach
                  @foreach( $intervals['afternoon'] as $key => $interval )
                    <div class="custom-control custom-radio mb-3">
                      <input name="scheduled_time" value="{{ $interval['start'] }}" class="custom-control-input" id="intervalMorning{{ $key }}" type="radio" required>
                      <label class="custom-control-label" for="intervalAfternoon{{ $key }}">{{ $interval['start'] }} - {{ $interval['end'] }}</label>
                    </div>
                  @endforeach
                @else
                  <div class="alert alert-info" role="alert">
                    Seleccion un medio y una fecha, para ver sus horas disponibles.
                  </div>
                @endif
              </div>
            </div>

            <a type="button" href="{{ url('/attention/'.$type->user->id) }}" class="btn btn-default"> Volver Atrás </a>
            <button id="btn-workday-confirm" type="button"
                data-toggle="modal" data-target="#modal-resumen-show" 
                class="btn btn-primary d-none">Reservar </button>
          </div>
        </form>
        
      </div>
    </div>



  <div class="modal fade" id="modal-resumen-show" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <!-- <div class="modal-header">
          <h4 class="modal-title">{{-- $type->user->name --}}</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
        </div> -->
        <form method="POST" action="{{-- route('admin.budgets.updateCode', $budget->id).'#budget' --}}" class="form-horizontal">
          {{ csrf_field() }}
          <div class="modal-body">

              <label class="col-xs-12"></label><br><br>

              <div class="row">
                <div class="col text-center">
                  <span class="text-center text-muted mb-0">Estas a un paso de terminar tu reserva</span><br><br>

                  <h2>DÍA Y HORA DE RESERVA:<br>
                   <strong id="reserva_dia"></strong></h2><br>
                  <span class="text-center text-muted mb-0">Revisa bien el dia y la hora de tu reserva.</span><br>
                  <span class="text-center text-muted mb-0">No olvides llegar 10 minutos antes.</span><br><br>
                  
                </div>
              </div>
          </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">Cancelar</button>
              <button id="resumen_edit_update" type="button" 
                    onclick="event.preventDefault();
                      document.getElementById('reservar_crear').submit();"
                  class="btn btn-primary">Confirmar Reserva</button>
          </div>
          
        </form> 
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script>

    $('#modal-resumen-show').on('shown.bs.modal', function(){
      var fecha = document.getElementById('date').value;
      var memo=document.getElementsByName('scheduled_time');
      for(i=0; i<memo.length; i++){
        if(memo[i].checked){
          //var memory=memo[i].checked;
          var memory=memo[i].value;
        }
      }
      var anio =  fecha.substring(0, 4);                    
      var mes =  fecha.substring(5,7);                    
      var dia = fecha.substring(8, 10);
      document.getElementById('reserva_dia').innerHTML = dia+'/'+mes+'/'+anio+' '+memory;
     
    });


  


    let $doctor, $type, $date, $specialty, $hours;
    let iRadio;
    const noHoursAlert = `<div class="alert alert-danger" role="alert">
        <strong>Lo sentimos!</strong> No se encontraron horas disponibles en el dia seleccionado.
      </div>`;

    $(function() {  
      $specialty = $('#specialty');
      $doctor = $('#doctor');
      $type = $('#type');
      $date = $('#date');
      $hours = $('#hours');

      $date.change(loadHours);
      loadHours();
    });

    function loadHours(){

      const selectedDate = $date.val();
      const doctorId = $doctor.val();
      const typeId = $type.val();
      const url = `/api/schedule/hours?date=${selectedDate}&doctor_id=${doctorId}&type_id=${typeId}`;
     
      $.getJSON(url, displayHours);
    }

    function displayHours(data){
      //  console.log(data);
      if( !data.morning && !data.afternoon || data.morning.length === 0 && data.afternoon.length === 0 ){
        $hours.html(noHoursAlert);
        document.getElementById("btn-workday-active").innerHTML="DESHABILITADO";
        document.getElementById("btn-workday-active").classList.remove('btn-success');
        document.getElementById("btn-workday-confirm").classList.add('d-none');
        return;
      }
      let htmlHours = '';
      iRadio = 0;
      if( data.morning ){
        const morning_intervals = data.morning;
        morning_intervals.forEach(interval => {
          htmlHours += getRadioIntervalHtml(interval);
          //console.log(`${interval.start} - ${interval.end}`);
        });
      }
      if( data.afternoon ){
        const afternoon_intervals = data.afternoon;
        afternoon_intervals.forEach(interval => {
          htmlHours += getRadioIntervalHtml(interval);
          //console.log(`${interval.start} - ${interval.end}`);
        });
      }
      $hours.html(htmlHours);
      document.getElementById("btn-workday-active").innerHTML="ACTIVO";
      document.getElementById("btn-workday-active").classList.add('btn-success');
      document.getElementById("btn-workday-confirm").classList.remove('d-none');
    }

    function getRadioIntervalHtml(interval) {
      const text = `${interval.start} - ${interval.end}`;
      return `<div class="custom-control custom-radio mb-3">
          <input name="scheduled_time" value="${interval.start}" class="custom-control-input" id="interval${iRadio}" type="radio">
          <label class="custom-control-label" for="interval${iRadio++}">${text}</label>
        </div>`;
    }

  </script>
@endsection
