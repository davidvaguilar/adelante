@extends('layouts.panel')

@section('content')
<div class="card shadow">
  <div class="card-header border-0">
    <div class="row align-items-center">
      <div class="col">
        <h3 class="mb-0">Seleccionar tipo de atención</h3>
      </div>
      <div class="col text-right">
        <a href="{{ url('types')}}" class="btn btn-sm btn-default">
          Cancelar y volver
        </a>
      </div>
    </div>
  </div>

  <div class="card-body">
 

    @if( $errors->any() )
      <div class="alert alert-danger" role="alert">
        <ul>
          @foreach( $errors->all() as $error )
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <form action="{{ url('/schedule') }}" method="GET">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="name">Tipo de atencion</label>
        <select name="type_id" id="type" class="form-control" data-style="btn-default" title="Seleccione una "
        onchange="loadHours()" required>
          @foreach ($types as $type)
            <option value="{{ $type->id }}">{{ $type->name }}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label for="name">Intervalo de Atención (minutos)</label>
        <input type="number" id="interval" name="interval" value="5" min="5" step="5" max="60" class="form-control" required>
      </div>

      
      <input type="hidden" id="doctor" name="user_id"  class="form-control" value="{{ $user->id }}" >
      
      <button type="submit" class="btn btn-primary"> Seleccionar </button>

    </form>

  </div>
</div>

<script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>

<script>
// let $doctor, $type;
  loadHours();

  //$(function() {
      //$doctor = $('#doctor');
 /*     $doctor = document.getElementById('doctor').value;
      alert($doctor)
      $type = $('#type');*/
     // loadHours();
   // });

  function loadHours(){
   
     var doctorId = document.getElementById('doctor').value;
     var typeId = document.getElementById('type').value;
    
      const url = `/api/typeuser/interval?doctor_id=${doctorId}&type_id=${typeId}`;
      $.getJSON(url, displayHours);
  }


  function displayHours(data){
      //if(data.)
      console.log(data[0].interval);
      if(data[0].interval){
        document.getElementById('interval').value = data[0].interval;
      } else {
        document.getElementById('interval').value = 5;
      }
      /*if( !data.morning && !data.afternoon || data.morning.length === 0 && data.afternoon.length === 0 ){
        $hours.html(noHoursAlert);
        return;
      }
      let htmlHours = '';
      iRadio = 0;
      if( data.morning ){
        const morning_intervals = data.morning;
        morning_intervals.forEach(interval => {
          htmlHours += getRadioIntervalHtml(interval);
          //console.log(`${interval.start} - ${interval.end}`);
        });
      }
      if( data.afternoon ){
        const afternoon_intervals = data.afternoon;
        afternoon_intervals.forEach(interval => {
          htmlHours += getRadioIntervalHtml(interval);
          //console.log(`${interval.start} - ${interval.end}`);
        });
      }
      $hours.html(htmlHours);*/
    }


</script>
@endsection
