<li class="nav-item">
  <a class="nav-link  text-white text-uppercase" href="/profile">
    <i class="fa fa-user"></i> Mi perfil
  </a>
</li>
<li class="nav-item">
  <a class="nav-link  text-white text-uppercase" href="/appointments">
    <i class="ni ni-time-alarm"></i> Mis reservas
  </a>
</li>

<li class="nav-item">
  <a class="nav-link  text-white text-uppercase" href="/"> <!-- href="/appointments/create" -->
    <i class="ni ni-send"></i> Reservar
  </a>
</li>
