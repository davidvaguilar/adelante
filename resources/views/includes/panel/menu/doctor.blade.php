<!--<li class="nav-item">
  <a class="nav-link" href="/schedule">
    <i class="ni ni-calendar-grid-58 text-danger"></i> Gestionar horario
  </a>
</li>-->
<li class="nav-item">
  <a class="nav-link text-white text-uppercase" href="/appointments">
    <i class="fa fa-list-ol"></i> Mis reservas
  </a>
</li>

<li class="nav-item">
  <a class="nav-link text-white text-uppercase" href="/users/{{ auth()->id() }}">
    <i class="fa fa-calendar-check"></i> Horario de atenciones
  </a>
</li>

<li class="nav-item">
  <a class="nav-link text-white text-uppercase" href="/qr_qenerate/{{ auth()->id() }}">
    <i class="fa fa-qrcode"></i> Codigo Qr Empresa
  </a>
</li>

<li class="nav-item">
  <a class="nav-link  text-white text-uppercase" href="/profile">
    <i class="fa fa-user"></i> Mi perfil
  </a>
</li>

<li class="nav-item">
  <a class="nav-link  text-white text-uppercase" href="/chart">
  <i class="fas fa-chart-bar"></i> Grafico
  </a>
</li>

<!-- <li class="nav-item">
  <a class="nav-link" href="/patients">
    <i class="ni ni-satisfied text-info"></i> Mis clientes
  </a>
</li>-->
