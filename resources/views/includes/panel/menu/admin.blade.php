<!-- <li class="nav-item">
  <a class="nav-link" href="#">
    <i class="ni ni-tv-2 text-danger"></i> Dashboard
  </a>
</li> -->
<li class="nav-item">
  <a class="nav-link text-white text-uppercase" href="/specialties">
    <i class="fa fa-cubes"></i> Categorias
  </a>
</li>
<li class="nav-item">
  <a class="nav-link text-white text-uppercase" href="/types">
    <i class="fa fa-list-alt"></i> Tipos de Atención
  </a>
</li>

<li class="nav-item">
  <a class="nav-link text-white text-uppercase" href="/doctors">
    <i class="fa fa-industry"></i> Empresas
  </a>
</li>

<li class="nav-item">
  <a class="nav-link  text-white text-uppercase" href="/profile">
    <i class="fa fa-user"></i> Mi perfil
  </a>
</li>

<!-- <li class="nav-item">
  <a class="nav-link" href="./patients">
    <i class="ni ni-satisfied text-info"></i> Usuarios
  </a>
</li> -->
<!-- <li class="nav-item">
  <a class="nav-link" href="/appointments">
    <i class="ni ni-time-alarm text-primary"></i> Reservas
  </a>
</li> -->
