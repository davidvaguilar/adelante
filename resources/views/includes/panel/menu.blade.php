<h6 class="navbar-heading  text-white text-uppercase">
  @if( auth()->user()->role == 'admin' )
    Gestión de datos
  @else
    Menú principal
  @endif
</h6>
<ul class="navbar-nav">
  @include('includes.panel.menu.'.auth()->user()->role)

  <li class="nav-item ">
   
    <a class="nav-link text-white text-uppercase" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('formLogout').submit();">
      <i class="fa fa-sign-out-alt"></i> Cerrar sesión
    </a>
    <form action="{{ route('logout') }}" method="POST" style="display: none;" id="formLogout">
      {{ csrf_field() }}
    </form>
  </li>
</ul>
@if( auth()->user()->role == 'admin' )
  <!--<hr class="my-3">
  <h6 class="navbar-heading text-muted">Reportes</h6>
  <ul class="navbar-nav mb-md-3">
    <li class="nav-item">
      <a class="nav-link" href="{{-- url('/charts/appointments/line') --}}">
        <i class="ni ni-sound-ware text-yellow"></i> Frecuencia de citas
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{-- url('/charts/doctors/column') --}}">
        <i class="ni ni-spaceship text-orange"></i> Medicos más activos
      </a>
    </li>
  </ul>  -->
@endif
