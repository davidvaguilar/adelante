<table>
    <thead> 
      <tr>
          <th colspan="7"><strong>HISTORIAL DE RESERVAS</strong></th>
      </tr>
    </thead>
    <thead> 
      <tr>
          <th colspan="7"></th>
      </tr>
    </thead>

    <thead class="thead-light">
      <tr>
        <th scope="col">Id Reserva</th>
        <th scope="col">Categoria</th>
      
          <th scope="col" style="min-width: 100px;">Cliente</th>
          <th scope="col" style="min-width: 100px;">Telefono</th>
     
        <th scope="col">Fecha</th>
        <th scope="col">Hora</th>
        <th scope="col">Estado</th>
      </tr>
    </thead>
    <tbody> 
      @foreach( $appointments as $appointment )
      <tr>
        <th scope="row">
          {{ $appointment->id }}
        </th>
        <th scope="row">
          {{ $appointment->specialty->name }}
        </th>
      
          <td>
            {{ $appointment->patient->name }}
          </td>
          <td>
            {{ $appointment->patient->phone }}
          </td>
      
        <td>
          {{ $appointment->scheduled_date->format('d-m-Y') }}
        </td>
        <td>
          {{ $appointment->scheduled_time_12 }}
        </td>
        <td>
          {{ $appointment->status }}
        </td>
      </tr>
      @endforeach
    </tbody>
</table>