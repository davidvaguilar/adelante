<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="description" content="">
    <script src="{{ asset('landing/themekit/scripts/jquery.min.js') }}"></script>
    <script src="{{ asset('landing/themekit/scripts/main.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/bootstrap-grid.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/style.css') }}">   <!-- MODIFICADO -->
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/glide.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/content-box.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/contact-form.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/media-box.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/skin.css') }}">     <!-- MODIFICADO -->
    <link rel="icon" href="{{ asset('img/adelante-ico.jpeg') }}">
    <link rel="stylesheet " href="{{ asset('landing/themekit/media/icons/iconsmind/line-icons.min.css') }} ">
    <link href="{{ asset('argon-dashboard/assets/vendor/select2/dist/css/select2.min.css') }}" rel="stylesheet">

</head>

<style>

    @media screen and (max-width: 600px) {
        #contrata-miplay{
            display:none;
        }
    }

</style>

<body>

    <div id="preloader"></div>

   


    <nav class="menu-classic menu-transparent menu-fixed menu-one-page align-right light" data-menu-anima="fade-bottom" data-scroll-detect="true">
        <div class="container">
            <div class="menu-brand"  >   <!-- style="height: 80px;" -->
                <a href="{{ url('/') }}">
                    <img class="logo-default" src="{{ asset('img/items/logo_nav.png') }}" alt="logo"  />
                    <img class="logo-retina" src="{{ asset('img/items/logo_nav.png') }}" alt="logo"  />
                   <!-- <img class="logo-retina" src="{{-- asset('img/items/logo_nav.png') --}}" alt="logo"  />  -->
                </a>
            </div>
           
            <i class="menu-btn"></i>
            <div class="menu-cnt">
                <ul>
                    <li class="d-none d-xl-block">
                        <a href="{{ url('/') }}">
                          <img src="{{ asset('landing/media/home_icon.png') }}" alt="" style="max-height: 30px; margin-top: 10px;">
                        </a>
                    </li>


                    
                    @guest
                    <li>
                        <a href="#about">¿POR QUÉ ADELANTE.CL?</a>
                    </li>
                    <li>
                        <a href="{{ url('register') }}">REGISTRATE</a>
                    </li>
                    <li>
                        <a href="{{ route('login') }}">INICIAR SESIÓN</a>
                    </li>
                    @else
                         @if( auth()->user()->role == 'doctor' )
                            <li class="nav-item">
                                <a class="nav-link text-white text-uppercase" href="/appointments">
                                    <i class="fa fa-list-ol"></i> Mis reservas
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-white text-uppercase" href="/users/{{ auth()->id() }}">
                                    <i class="fa fa-calendar-check"></i> Horario de atenciones
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link text-white text-uppercase" href="/qr_qenerate/{{ auth()->id() }}">
                                    <i class="fa fa-qrcode"></i> Codigo Qr Empresa
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link  text-white text-uppercase" href="/profile">
                                    <i class="fa fa-user"></i> Mi perfil
                                </a>
                            </li>
                        @else
                            <li>
                                <a href="#about">¿POR QUÉ ADELANTE.CL?</a>
                            </li>
                            <li>
                                <a href="{{ url('profile') }}" style="padding: 0px 0px 0px 30px;" >
                                    @if( Auth::user()->avatar )
                                        <img src="{{ Auth::user()->avatar }}" alt="Imagen de perfil" height="28">
                                    @else
                                    <img src="{{ asset('img/adelante-ico.jpeg') }}" alt="Imagen de perfil" style="max-height: 35px; margin-top: 10px;">
                                    @endif
                                </a>
                            </li>

                            <li>
                                <a href="{{ url('profile') }}" style="padding: 0px 0px 0px 0px;" >
                                    {{ Auth::user()->name }} 
                                </a>
                            </li>
                        @endif
                    @endguest
                </ul>
                <!-- <div class="menu-right">
                    <div class="menu-custom-area">
                    </div>
                </div>-->
                <div class="clear"></div>
            </div>
            
        </div>
       
    </nav>
   
    @yield('content')   

    <!-- <a href="{{-- url('register') --}}" id="contrata-miplay" 
        target="_blank"
        style="top:60px; position:fixed; right:0; width:300px;">

        <img src="{{-- asset('landing/banner300x600.png') --}}" width="300px" alt="whatapp">
    </a>-->

    <i class="scroll-top-btn scroll-top show"></i>




    <footer class="light">
        <section class="section-image light align-center " 
            style="background-color: rgb(81, 137, 212);background-image:url({{ asset('landing/media/fondo.png') }}); ">
            <!-- <div class="container">  -->
                <img src="{{ asset('img/items/logo_nav.png') }}"  style="padding-top: 20px;">
                <div class="col-lg-4" style="width:60%; text-align:center; margin: auto; padding: 20px;">
                    <div class="icon-links icon-social social-colors align-center align-center-md"> 
                        <a class="facebook"><img src="{{ asset('landing/media/fb.png') }}" alt=""></a>
                        <a class="twitter"><img src="{{ asset('landing/media/ig.png') }}" alt=""></a>
                        <a class="linkedin"><img src="{{ asset('landing/media/tw.png') }}" alt=""></a>
                        <a class="pinterest"><img src="{{ asset('landing/media/yt.png') }}" alt=""></a>
                    </div>
                </div>
            <!-- </div>  -->
        </section>
    </footer>
        
    <section class="section-base">
      <div class="container" style="padding: 20px;">            
        <table class="table" style="color: #5596D6;">
            <tbody>
                <tr>
                    <td>
                        <h3 style="color: #5596D6;"><strong>COMERCIAL</strong></h3>
                        Productos<br>
                        Contacta con un vendedor
                    </td>
                    <td>
                        <h3 style="color: #5596D6;"><strong>SOPORTE</strong></h3>
                        +562 26665563<br>
                        soporte@adelanteya.cl
                    </td>
                   
                    <td style="padding: 0px; text-align: center">
                        <img src="{{ asset('img/items/corfo.png') }}" alt="imagen corfo">
                    </td>
                    <td style="padding: 0px; text-align: center">
                        <a href="{{ url('register') }}" target="_blank">
                            <img src="{{ asset('landing/banner250x250.png') }}" height="200px" style="border-radius: 10%;" alt="imagen banigualdad"/>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>

      </div>
    </section>
    <footer class="light">
        <section class="section-image light align-center" 
            style="background-color: rgb(88, 153, 217);background-image:url({{ asset('landing/media/fondo.png') }}); ">
            <div class="container" style="padding: 20px;">
                <p>Todos los derechos reservados © <?php print(date('Y')); ?> Adelanteya.cl </p>
                <br>
            </div>
        </section>
    </footer>
    
    <script src="{{ asset('landing/themekit/scripts/parallax.min.js') }} "></script>
    <script src="{{ asset('landing/themekit/scripts/glide.min.js') }} "></script>
    <script src="{{ asset('landing/themekit/scripts/magnific-popup.min.js') }} "></script>
    <script src="{{ asset('landing/themekit/scripts/tab-accordion.js') }} "></script>
    <script src="{{ asset('landing/themekit/scripts/imagesloaded.min.js') }} "></script>
    <script src="{{ asset('landing/themekit/scripts/progress.js') }} "></script>
    <!-- <script src="{{ asset('landing/media/custom.js') }} "></script>      NO EXISTE -->
    
    <script src="{{ asset('argon-dashboard/assets/vendor/select2/dist/js/select2.full.min.js') }}"></script>



    <script>

      $('.select2').select2();

    </script>
</body>

</html>