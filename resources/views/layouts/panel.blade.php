<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>{{ config('app.name', 'Laravel') }} </title>
  <!-- Sistema de Reserva de Citas | --> {{-- config('app.name') --}}
  <!-- Favicon -->
  <link href="{{ asset('img/adelante-ico.jpeg') }}" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">


  
  <link href="{{ asset('vendor/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendor/datatables.net-bs/css/dataTables.bootstrap.css') }}" rel="stylesheet">

  <style>
.form-inline {
    width: 100%;
    flex-flow: row wrap;
    align-items: center;
}
  </style>

  <!-- Icons -->
  <link href="{{ asset('vendor/nucleo/css/nucleo.css') }}" rel="stylesheet">
  <link href="{{ asset('vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="{{ asset('css/argon.css?v=1.0.0') }}" rel="stylesheet">
  @yield('styles')

</head>


<body>
  <!-- Sidenav -->
  <nav class="navbar navbar-vertical fixed-left navbar-expand-md " style="background: rgb(55,59,195);
    background: linear-gradient(180deg, rgba(55,59,195,1) 35%, rgba(83,136,214,1) 100%);" id="sidenav-main">   <!--  navbar-light  -->
    <div class="container-fluid">
      <!-- Toggler -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">

        <i class="fa fa-bars text-white"></i>
      </button>
      <!-- Brand  pt-0 -->
      <a class="navbar-brand" href="{{ url('/') }}">
        <img src="{{ asset('img/items/logo_nav.png') }}" class="navbar-brand-img" alt="...">
      </a>
      <!-- User -->
      <ul class="nav align-items-center d-md-none">
    
        <li class="nav-item dropdown">
          <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="Image placeholder" src="{{ asset('img/adelante-ico.jpeg') }}">
              </span>
            </div>
          </a>
        {{--  @include('includes.panel.dropdown_menu')    --}}
        </li>
      </ul>
      <!-- Collapse MENU MOVIL -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Collapse header -->
        <div class="navbar-collapse-header d-md-none">
          <div class="row">
            <div class="col-6 collapse-brand">
        
                <img src="{{ asset('img/adelanteya.png') }}">
          
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        @include('includes.panel.menu')   
      </div>
    </div>
  </nav>
  <!-- Main content -->
  <div class="main-content" >
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-dark" id="navbar-main"> <!--  navbar-expand-md  -->
      <div class="container-fluid">
        <!-- Brand -->
        <span class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="{{ url('/') }}">Panel de Administración</span>

        
        <!-- Form -->
        <!--<form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
          </div>
        </form>-->
        <!-- User -->

        <ul class="navbar-nav align-items-center d-none  d-lg-inline-block ">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="{{ asset('img/adelante-ico.jpeg') }}">
                </span>
                <div class="media-body ml-2 d-lg-block">
                  <span class="mb-0 text-sm font-weight-bold">{{ auth()->user()->name }}</span>
                </div>
              </div>
            </a>
          {{--     @include('includes.panel.dropdown_menu')   --}}
          </li>
        </ul>


      </div>
    </nav>
    <!-- Header -->
    <div class="header pb-8 pt-4 pt-md-6" style="background: rgb(55,59,195);">

    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
      @yield('content')
      <!-- Footer -->
      @include('includes.panel.footer')
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>

  <!-- <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script> -->

  <script src="{{ asset('vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('vendor/datatables.net-bs/js/dataTables.bootstrap.js') }}"></script>

  <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
  
  <!-- Optional JS -->
  <script src="{{ asset('vendor/chart.js/dist/Chart.min.js') }}"></script>
  <script src="{{ asset('vendor/chart.js/dist/Chart.extension.js') }}"></script>
  @yield('scripts')
  <!-- Argon JS -->
  <script src="{{ asset('js/argon.js?v=1.0.0') }}"></script>

  <script>
    $(function () {
      $('#example1').DataTable({
        'paging'      : false,
        'lengthChange': false,
        'searching'   : true,
        'ordering'    : false,
        'info'        : false,
        'autoWidth'   : false,
        language: {
          "decimal": "",
          "emptyTable": "No hay información",
          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
          "infoPostFix": "",
          "thousands": ",",
          "lengthMenu": "Mostrar _MENU_ Entradas",
          "loadingRecords": "Cargando...",
          "processing": "Procesando...",
          "search": "Buscar:",
          "zeroRecords": "Sin resultados encontrados",
          "paginate": {
              "first": "Primero",
              "last": "Ultimo",
              "next": "Siguiente",
              "previous": "Anterior"
          }
        },
      })

    })
  </script>
</body>

</html>
