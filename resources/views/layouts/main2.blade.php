<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>{{ config('app.name', 'Laravel') }} </title>
  <!-- Sistema de Reserva de Citas | --> {{-- config('app.name') --}}
  <!-- Favicon -->
  <link rel="icon" href="{{ asset('img/adelante-ico.jpeg') }}">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="{{ asset('vendor/nucleo/css/nucleo.css') }}" rel="stylesheet">
  <link href="{{ asset('vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="{{ asset('css/argon.css?v=1.0.0') }}" rel="stylesheet">
  {{-- @yield('styles') --}}
</head>

<style>/*
.col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col, .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm, .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md, .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg, .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl, .col-xl-auto{
    position: relative;
    
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
}*/

</style>

<body>
  <!-- Sidenav -->
  <nav class="navbar navbar-vertical fixed-left navbar-expand-md " style="background: rgb(55,59,195);
    background: linear-gradient(180deg, rgba(55,59,195,1) 35%, rgba(83,136,214,1) 100%);" id="sidenav-main">   
  
    <div class="container-fluid">
    
      <!-- Brand  pt-0-->
      <a class="navbar-brand collapse" href="{{ url('/') }}">
        <img src="{{ asset('img/items/logo_nav.png') }}" class="navbar-brand-img" alt="...">
      </a>
      <!-- User -->
   

      <!--<ul class="nav align-items-center d-md-none">
        <li class="nav-item dropdown">
          <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
            <div class="media align-items-center" >
              <span class="avatar avatar-sm rounded-circle">
                <img alt="Image placeholder" src="{{-- auth()->user()->avatar ?  auth()->user()->avatar : asset('img/adelante-ico.jpeg') --}}" >
             
              </span>
            </div>
          </a>
          
        </li>
      </ul>-->


      <!-- Collapse   style="margin:30px;"-->
      <div class="collapse navbar-collapse">

        <div class="text-center">
          <img src="{{ $type->user ? asset($type->user->url) : asset('img/adelante-ico.jpeg') }}"  height="120px" style="border-radius: 100%;" />
        </div>
        <br>
        <br>
        <span class="h4 mb-0 text-white text-uppercase d-lg-inline-block">{{ $type->user->name }}</span><br>
        <span class="h4 mb-0 text-white d-lg-inline-block"><strong>{{ $type->user->address ? "Dirección:" : '' }}</strong></span>
        <span class="h6 text-white">{{ $type->user->address }}</span>

        <span class="h4 mb-0 text-white d-lg-inline-block"><strong>{{ $type->user->phone ? "Teléfono:" : '' }}</strong></span>
        <span class="h6 text-white">{{ $type->user->phone }}</span>

        <span class="h4 mb-0 text-white d-lg-inline-block"><strong>{{ $type->user->description ? "Horarios:" : '' }}</strong></span>
        <span class="h6 text-white">{{ $type->user->description }}</span>

        <button type="button" id="btn-workday-active"
            class="btn btn-sm">-</button>

      </div>
      <div class="collapse navbar-collapse " style="position:  fixed;  bottom: 20px; " >
        <a href="{{ route('logout') }}" class="text-white btn-block"
            onclick="event.preventDefault(); document.getElementById('formLogout').submit();">
            <i class="fa fa-sign-out-alt fa-2x"></i>
          <span>CERRAR SESIÓN</span>
        </a>
      </div>

       <!-- Collapse   style="margin:30px;"-->
        <!-- Para dispositivos pequeños-->
        <div class="toggler">

            <div class="text-center">
                <img src="{{ $type->user ? asset($type->user->url) : asset('img/adelante-ico.jpeg') }}" height="80px" style="border-radius: 100%; margin: 10%;" />
                <span class="h4 mb-0 text-white text-uppercase d-lg-inline-block"><strong>NOTARIA POZA</strong></span>
            </div>
            <span class="h4 mb-0 text-white d-lg-inline-block"><strong>Dirección:&nbsp;</strong><span class="h6 text-white">Anibal Pinto 515, Iquique, Tarapacá</span></span><br>
            <span class="h4 mb-0 text-white d-lg-inline-block"><strong>Teléfono:&nbsp;</strong><span class="h6 text-white">(57) 257 3535</span></span><br>
            <span class="h4 mb-0 text-white d-lg-inline-block"><strong>Horarios:&nbsp;</strong><span class="h6 text-white">Lunes a Sábado de 08:00 a 16:00 hrs.</span></span><br>
            <br>

        </div>
        <div class="collapse navbar-collapse " style="position:  fixed;  bottom: 20px; ">
            <a href="#" class="text-white btn-block" onclick="event.preventDefault(); document.getElementById('formLogout').submit();">
                <i class="fa fa-sign-out-alt fa-2x"></i>
                <span>CERRAR SESIÓN</span>
            </a>
        </div>
   

    </div>
  </nav>


  <div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase" href="{{ url('/') }}"><!-- Panel de Administración --></a>

        <ul>
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="{{ auth()->user()->avatar ? auth()->user()->avatar : asset('img/adelante-ico.jpeg') }}">
                </span>
                <div class="media-body ml-2 d-lg-block">
                  <span class="mb-0 text-sm font-weight-bold">{{ auth()->user()->name }}</span>
                </div>
              </div>
            </a>
            

            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <div class="dropdown-header noti-title">
                <h6 class="text-overflow m-0">Bienvenido!</h6>
              </div>
              <a href="/profile" class="dropdown-item">
                <i class="ni ni-single-02"></i>
                <span>Mi perfil</span>
              </a>
              <a href="/appointments" class="dropdown-item">
                <i class="ni ni-calendar-grid-58"></i>
                <span>Mis reservas</span>
              </a>

              <div class="dropdown-divider"></div>
              <a href="{{ route('logout') }}" class="dropdown-item"
                  onclick="event.preventDefault(); document.getElementById('formLogout').submit();">
                <i class="fa fa-sign-out-alt"></i>
                <span>Cerrar sesión</span>
              </a>
              <form action="{{ route('logout') }}" method="POST" style="display: none;" id="formLogout">
                {{ csrf_field() }}
              </form>
            </div>

          </li>
        </ul>
      </div>
    </nav>


  
    @yield('content')

  </div>
  
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
 
  <!-- <script src="{{ asset('/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  Argon JS -->
  <script src="{{ asset('js/argon.js?v=1.0.0') }}"></script>
 
  @yield('scripts') 
  <script>


</script>
</body>

</html>
