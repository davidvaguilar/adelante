<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="description" content="">
    
    <link type="text/css" href="{{ asset('css/argon.css?v=1.0.0') }}" rel="stylesheet">
    <link href="{{ asset('vendor/nucleo/css/nucleo.css') }}" rel="stylesheet">

    <script src="{{ asset('landing/themekit/scripts/jquery.min.js') }}"></script>
    <script src="{{ asset('landing/themekit/scripts/main.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/bootstrap-grid.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/glide.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/content-box.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/contact-form.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/media-box.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/skin.css') }}">
    <link rel="icon" href="{{ asset('img/adelante-ico.jpeg') }}">
    

</head>


<body>
    <div id="preloader"></div>
    <nav class="menu-classic menu-transparent menu-fixed menu-one-page align-right light" data-menu-anima="fade-bottom" data-scroll-detect="true">
        <div class="container">
            <div class="menu-brand">  <!--   style="height: 80px;" -->
                <a href="{{ url('/') }}">
                    <img class="logo-default" src="{{ asset('img/items/logo_nav.png') }}" alt="logo" />
                    <img class="logo-retina" src="{{ asset('img/items/logo_nav.png') }}" alt="logo" />
                </a>
            </div>
            <i class="menu-btn"></i>
            <div class="menu-cnt">
                <ul>
                    <li>
                        <a href="{{ url('/') }}">
                          <img src="{{ asset('landing/media/home_icon.png') }}" alt="" style="max-height: 30px; margin-top: 10px;">
                        </a>
                    </li>
                    <li>
                        <a href="#about">¿POR QUÉ ADELANTE.CL?</a>
                    </li>
                   
                    @guest
                    <li>
                        <a href="{{ url('register') }}">REGISTRATE</a>
                    </li>
                    <li>
                        <a href="{{ route('login') }}">INICIAR SESIÓN</a>
                    </li>
                    @else
                    <li>
                        <a href="{{ url('profile') }}" style="padding: 0px 0px 0px 30px;" >
                            @if( Auth::user()->avatar )
                                <img src="{{ Auth::user()->avatar }}" alt="Imagen de perfil" height="28">
                            @else
                              <img src="{{ asset('img/adelante-ico.jpeg') }}" alt="Imagen de perfil" style="max-height: 35px; margin-top: 10px;">
                            @endif
                        </a>
                    </li>

                    <li>
                        <a   href="{{ url('profile') }}" style="padding: 0px 0px 0px 0px;" >
                            {{ Auth::user()->name }} 
                        </a>
                    </li>
                    @endguest
                </ul>
                <div class="menu-right">
                    <div class="menu-custom-area">
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </nav>


    <main>
        <section class="section-image section-full-width-right light no-padding-top"  
              style="background-color: rgb(55,59,195);background-image:url({{ asset('landing/media/fondo.png') }});"> <!--  section-bottom-layer -->
            <div class="container">
                <div class="row">
                  <div class="col-lg-12" data-anima="fade-in" data-time="1000">
                  <hr class="space-lg" />
                    @yield('content')
                </div>
                </div>
            </div>
        </section>
     
    </main>
 

    <i class="scroll-top-btn scroll-top show"></i>
    <footer class="light">
        <section class="section-image light align-center " 
            style="background-color: rgb(81, 137, 212);background-image:url({{ asset('landing/media/fondo.png') }}); ">
            <!-- <div class="container">  -->
                <img src="{{ asset('img/items/logo_nav.png') }}"  style="padding-top: 20px;">
                <div class="col-lg-4" style="width:60%; text-align:center; margin: auto; padding: 20px;">
                    <div class="icon-links icon-social social-colors align-center align-center-md"> 
                        <a class="facebook"><img src="{{ asset('landing/media/fb.png') }}" alt=""></a>
                        <a class="twitter"><img src="{{ asset('landing/media/ig.png') }}" alt=""></a>
                        <a class="linkedin"><img src="{{ asset('landing/media/tw.png') }}" alt=""></a>
                        <a class="pinterest"><img src="{{ asset('landing/media/yt.png') }}" alt=""></a>
                    </div>
                </div>
            <!-- </div>  -->
        </section>
    </footer>
        
    <section class="section-base">
      <div class="container" style="padding: 20px;">            
        <table class="table" style="color: #5596D6;">
            <tbody>
                <tr>
                    <td>
                        <h3 style="color: #5596D6;"><strong>COMERCIAL</strong></h3>
                        Productos<br>
                        Contacta con un vendedor
                    </td>
                    <td>
                        <h3 style="color: #5596D6;"><strong>SOPORTE</strong></h3>
                        +562 26665563<br>
                        soporte@adelanteya.cl
                    </td>
                   
                    <td style="padding: 0px;">
                        <img src="{{ asset('img/items/corfo.png') }}" alt="imagen corfo" >
                    </td>
                    <td style="padding: 0px; text-align: center">
                        <a href="{{ url('register') }}" target="_blank">
                            <img src="{{ asset('landing/banner250x250.png') }}" height="200px" style="border-radius: 10%;" alt="imagen banigualdad"/>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>

      </div>
    </section>
    <footer class="light">
        <section class="section-image light align-center" 
            style="background-color: rgb(88, 153, 217);background-image:url({{ asset('landing/media/fondo.png') }}); ">
            <div class="container" style="padding: 20px;">

                <p>Todos los derechos reservados © <?php print(date('Y')); ?> Adelanteya.cl </p>
                <br>
            </div>
        </section>
    </footer>


</body>

</html>