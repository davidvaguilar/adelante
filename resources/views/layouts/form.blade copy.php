<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>{{ config('app.name') }} | @yield('title')</title>
  <!-- Favicon -->
  <link href="{{ asset('img/adlinks-ico.jpg') }}" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="{{ asset('vendor/nucleo/css/nucleo.css') }}" rel="stylesheet">
  <link href="{{ asset('vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="{{ asset('css/argon.css?v=1.0.0') }}" rel="stylesheet">
</head>


<style>
  
    
  .bg-default2{
      
        background: #373bc3;
        background-image: url("{{ asset('img/items/fondo.png') }}"); 
                    background-repeat: no-repeat;
                    background-size: cover; 
                   opacity: 1.0;
    }

  </style>

<body class="bg-default2">
  <div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark">
      <div class="container px-4">
        <a href="{{ url('/') }}">
            <img src="{{ asset('img/items/logo_nav.png') }}" width="150px">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse-main" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar-collapse-main">
          <!-- Collapse header -->
          <div class="navbar-collapse-header d-md-none">
            <div class="row">
              <div class="col-6 collapse-brand">
                <a href="{{ url('/') }}">
                  <img src="{{ asset('img/items/logo_nav.png') }}">
                </a>
              </div>
              <div class="col-6 collapse-close">
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                  <span></span>
                  <span></span>
                </button>
              </div>
            </div>
          </div>
          <!-- Navbar items -->
          <ul class="navbar-nav ml-auto">   
              
            <li class="nav-item">
              <a class="nav-link nav-link-icon" href="https://github.com/creativetimofficial/argon-design-system"  title="Star us on Github">
                <i class="fa fa-home fa-2x"></i>  
              </a>
            </li>
            <li class="nav-item d-none d-lg-block">
              <a href="https://www.creative-tim.com/product/argon-design-system-pro?ref=ads-upgrade-pro" target="_blank" class="btn btn-primary btn-icon">
                <span class="nav-link-inner--text">¿POR QUÉ ADELANTE.CL?</span>
              </a>
            </li>
            <li class="nav-item d-none d-lg-block">
              <a href="{{ url('register') }}" class="btn btn-primary btn-icon">
                <span class="nav-link-inner--text">REGISTRATE</span>
              </a>
            </li>
           
          </ul>
        </div>
      </div>
    </nav>
   
  
    
  </div>
  <hr>
  <!-- Header -->
  <div class="py-4 py-lg-5">
    <div class="container">
      <div class="header-body text-center mb-7">
        <div class="row justify-content-center">
          <div class="col-lg-5 col-md-6">
            <h1 class="text-white">
              @yield('title', 'Bienvenido!')
            </h1>
            <p class="text-lead text-light">
              @yield('subtitle')
            </p>
          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- Page content -->
    @yield('content')
  <!-- Footer -->
 <!--  <footer class="py-5">
    <div class="container">
      <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-6">
          <div class="copyright text-center text-xl-left text-muted">
            &copy; <?php //echo date('Y'); ?> <a href="/" class="font-weight-bold ml-1" target="_blank">{{ config('app.name') }}</a>
          </div>


        </div>
        <div class="col-xl-6">
          <ul class="nav nav-footer justify-content-center justify-content-xl-end">
      
            <li class="nav-item">
              <a href="#" class="nav-link" target="_blank">Acerca de</a>
            </li>

          </ul>
        </div>
      </div>
    </div>
  </footer>
  Argon Scripts -->
  <!-- Core -->
  <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
  <!-- Argon JS -->
  <script src="{{ asset('js/argon.js?v=1.0.0') }}"></script>
</body>

</html>
