

@extends('layouts.main2')

@section('content')

    <!-- Header -->
    <div class="header pb-5 pt-4 pt-md-4">

    </div>
    <!-- Page content -->
    <div class="container ">

      <h3 class="navbar-heading text-uppercase">
        <strong>INICIO / {{ $type->user->specialties->first()->name }} / <span class="text-primary">{{ $type->user->name }} </span> </strong>
      </h3>
                
      <div class="row justify-content-md-center">        
        <div class="col-lg-12 mb-lg-auto text-center">
            
            <br><br><br>
      
            @foreach($typeusers as $key => $typeuser)   
              <a href="{{ url('/workday/'.$typeuser->id) }}" class="btn btn-primary text-uppercase ">{{ $typeuser->type->name }}</a >
            @endforeach
        </div>
       
        
      </div>
    </div>
  

@endsection

@section('scripts')
  <script>
    document.getElementById("btn-workday-active").innerHTML="ACTIVO";
    document.getElementById("btn-workday-active").classList.add('btn-success');

    
  </script>
@endsection
