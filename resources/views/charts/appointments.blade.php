@extends('layouts.panel')

@section('content')
<div class="card shadow">
  <div class="card-header border-0">
    <div class="row align-items-center">
      <div class="col">
        <h3 class="mb-0">Reporte: Frecuencia de citas</h3>
      </div>
    </div>
  </div>

  <div class="card-body">
    <canvas id="bar-orders" style="height:330px"></canvas>
  </div>

</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/chart.js/Chart.js') }}"></script>

    <script>

    var areaChartData = {
      labels: {!! $orders_dates !!},
      datasets: [
        {
          label: 'Ordenes de trabajo',
          fillColor           : '#36a2eb',
          strokeColor         : '#36a2eb',
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data: {!! $orders_quantity !!},
        }
      ]
    }

//-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas                   = $('#bar-orders').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = areaChartData
    var barChartOptions                  = {
      scaleBeginAtZero        : true,
      scaleShowGridLines      : true,
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      scaleGridLineWidth      : 1,
      scaleShowHorizontalLines: true,
      scaleShowVerticalLines  : true,
      barShowStroke           : true,
      barStrokeWidth          : 2,
      barValueSpacing         : 5,
      barDatasetSpacing       : 1,
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)
      


    </script>
@endsection