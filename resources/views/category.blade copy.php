<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="description" content="">
    <script src="{{ asset('landing/themekit/scripts/jquery.min.js') }}"></script>
    <script src="{{ asset('landing/themekit/scripts/main.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/bootstrap-grid.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/glide.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/content-box.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/contact-form.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/themekit/css/media-box.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/skin.css') }}">
    <link rel="icon" href="{{ asset('img/adelante-ico.jpeg') }}">

    <link href="{{ asset('argon-dashboard/assets/vendor/select2/dist/css/select2.min.css') }}" rel="stylesheet">
</head>

<body>
    <div id="preloader"></div>
    <nav class="menu-classic menu-transparent menu-fixed menu-one-page align-right light" data-menu-anima="fade-bottom" data-scroll-detect="true">
        <div class="container">
            <div class="menu-brand">
                <a href="#">
                    <img class="logo-default" src="{{ asset('img/items/logo_nav.png') }}" alt="logo" />
                    <img class="logo-retina" src="{{ asset('img/items/logo_nav.png') }}" alt="logo" />
                </a>
            </div>
            <i class="menu-btn"></i>
            <div class="menu-cnt">
                <ul>
                    <li>
                        <a href="{{ url('/') }}">
                          <img src="{{ asset('landing/media/home_icon.png') }}" alt="" style="max-height: 30px; margin-top: 10px;">
                        </a>
                    </li>
                    <li>
                        <a href="">¿POR QUÉ ADELANTE.CL?</a>

                    </li>
                  
                    @guest
                    <li>
                        <a href="{{ url('register') }}">REGISTRATE</a>

                    </li>
                    <li>
                        <a href="{{ route('login') }}">INICIAR SESIÓN</a>

                    </li>
                    @else
                    <li>
                        <a href="{{ url('profile') }}" style="padding: 0px 0px 0px 30px;" >
                            @if( Auth::user()->avatar )
                                <img src="{{ Auth::user()->avatar }}" alt="Imagen de perfil" height="28">
                            @else
                              <img src="{{ asset('img/adelante-ico.jpeg') }}" alt="Imagen de perfil" style="max-height: 35px; margin-top: 10px;">
                            @endif
                        </a>
                    </li>

                    <li>
                        <a   href="{{ url('profile') }}" style="padding: 0px 0px 0px 0px;" >
                            {{ Auth::user()->name }} 
                            
                        </a>


                    </li>
                    @endguest
                </ul>
                <div class="menu-right">
                    <div class="menu-custom-area">
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </nav>


    <main>
        <section class="section-image section-full-width-right light no-padding-top"  
              style="background-color: rgb(41, 41, 124);background-image:url({{ asset('landing/media/fondo.png') }});"> <!--  section-bottom-layer -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12" data-anima="fade-in" data-time="1000">
                        <hr class="space" />
                        <hr class="space-lg" />
                        <h2 style="text-align: center;">¿QUÉ TRÁMITE DESEA REALIZAR?</h2>
                        <!--  class="mt-4 mb-3" <input class="input-text" type="text" placeholder="" style="width: 50%;margin-left: 25%;margin-right: 25%;"> -->
                        <form style="margin-left: 15%;margin-right:15%;" action="{{ url('/attention') }}" method="GET">
                            <select id="job_create_branch" name="branch_id"  onchange="this.form.submit()"
                                        class="input-text select2" style="width: 100%;margin-left: 15%;margin-right: 15%;">
                                <option value="">Busca una sucursal</option>
                                @foreach ($branches as $branch)
                                    <option value="{{ $branch->id }}">
                                    {{ $branch->name }}</option>
                                @endforeach
                          </select>
                        </form>
                        <br><br><br>
                     
                        <table class="table table-grid table-md-6">
                          <tbody>
                            <tr>
                              @foreach($category->users as $key => $user)  
                                @if( $user->url )
                                  <td style="text-align: center;">
                                  
                                    <a href="{{ url('/attention/'.$user->id) }}">
                                      <img src="{{ url($user->url) }}"  style="border-radius: 2%;"/><br>
                                      {{-- $user->name --}}
                                    </a>
                                    
                                  </td>
                                  @if( ($key % 2) == 1 )
                                      </tr>
                                  </tbody>
                                  <tbody>
                                      <tr>
                                  @endif

                                @endif
                              @endforeach

                            </tr>
                          </tbody>
                        </table><br><br><br><br>
                
                    </div>
                </div>
        </section>
     
    </main>
 
    <footer class="light">
        <section class="section-image light align-center " 
            style="background-color: rgb(56, 117, 167);background-image:url({{ asset('landing/media/fondo.png') }}); ">
            <div class="container ">
                <img src="{{ asset('landing/media/logo_footer.png') }}" alt="" style="padding: 25px;">
                <div class="col-lg-4" style="text-align: center; margin-left: 25%;">
                    <div class="icon-links icon-social social-colors align-right align-left-md">
                        <a class="facebook"><img src="{{ asset('landing/media/fb.png') }}" alt=""></a>
                        <a class="twitter"><img src="{{ asset('landing/media/ig.png') }}" alt=""></a>
                        <a class="linkedin"><img src="{{ asset('landing/media/tw.png') }}" alt=""></a>
                        <a class="pinterest"><img src="{{ asset('landing/media/yt.png') }}" alt=""></a>
                    </div>
                </div><br>
                <table class="table table-grid table-md-6">
                    <tbody>
                        <tr>
                            <td></td>
                            <td style="text-align: center;">
                                <h2>COMERCIAL</h2>
                                <h4>Productos
                                    <h4>Cotiza con un vendedor
                            </td>
                            <td style="text-align: center;">
                                <h2>SOPORTE</h2>
                                <h4>+56 9 26665563
                                    <h4>soporte@adelante.cl
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table><br><br>
                <p>Todos los derechos reservados ©2021 Adelante.cl </p>
                <br><br>
            </div>
        </section>
        <link rel="stylesheet " href="{{ asset('landing/themekit/media/icons/iconsmind/line-icons.min.css') }} ">
        <script src="{{ asset('landing/themekit/scripts/parallax.min.js') }} "></script>
        <script src="{{ asset('landing/themekit/scripts/glide.min.js') }} "></script>
        <script src="{{ asset('landing/themekit/scripts/magnific-popup.min.js') }} "></script>
        <script src="{{ asset('landing/themekit/scripts/tab-accordion.js') }} "></script>
        <script src="{{ asset('landing/themekit/scripts/imagesloaded.min.js') }} "></script>
        <script src="{{ asset('landing/themekit/scripts/progress.js') }} "></script>
        <script src="{{ asset('landing/media/custom.js') }} "></script>
        
        <script src="{{ asset('argon-dashboard/assets/vendor/select2/dist/js/select2.full.min.js') }}"></script>
    </footer>

    <script>

      $('.select2').select2();

    </script>
</body>

</html>