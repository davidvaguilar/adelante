<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/brand/favicon.png') }}">
 
  <link href="{{ asset('img/brand/favicon.png') }}" rel="icon" type="image/png">
  <title>{{ config('app.name', 'Laravel') }}</title>

  <!--  Buscadores tags   -->
  <meta name="keywords" content="adelante, reserva, cita, citacion, reservacion, gestion, atencion, comercio, conectado, corfo" />
  <meta name="description" content="Un sistema de gestión de reservas inteligentes. Enamora a tus clientes con una atención unica. Tu comercio ahora más conectado. Un proyecto financiado por CORFO" />

  <!--     Fonts and icons   
  <link href="{{ asset('prueba/css') }}" rel="stylesheet">
  <link href="{{ asset('prueba/all.css') }}" rel="stylesheet">  -->
  <!-- Nucleo Icons 
  <link href="{{ asset('prueba/nucleo-icons.css') }}" rel="stylesheet">
  <link href="{{ asset('prueba/nucleo-svg.css') }}" rel="stylesheet">-->
  <!-- Font Awesome Icons
  <link href="{{ asset('prueba/font-awesome.css') }}" rel="stylesheet">  -->
  <!-- CSS Files -->
  <link href="{{ asset('css/argon-design-system.css') }}" rel="stylesheet"> 

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

  <!-- Icons -->
  <link href="{{ asset('vendor/nucleo/css/nucleo.css') }}" rel="stylesheet">
  <link href="{{ asset('vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">

   <!-- Argon CSS
   <link type="text/css" href="{{ asset('/css/argon.css') }}" rel="stylesheet">  -->

  <style>
    .tt-query, /* UPDATE: newer versions use tt-input instead of tt-query */
    .tt-hint {
        width: 396px;
        height: 30px;
        padding: 8px 12px;
        font-size: 24px;
        line-height: 30px;
        border: 2px solid #ccc;
        border-radius: 8px;
        outline: none;
    }

    .tt-query { /* UPDATE: newer versions use tt-input instead of tt-query */
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    }

    .tt-hint {
        color: #999;
    }

    .tt-menu { /* UPDATE: newer versions use tt-menu instead of tt-dropdown-menu */
        width: 422px;
        margin-top: 12px;
        padding: 8px 0;
        background-color: #fff;
        border: 1px solid #ccc;
        border: 1px solid rgba(0, 0, 0, 0.2);
        border-radius: 8px;
        box-shadow: 0 5px 10px rgba(0,0,0,.2);
    }

    .tt-suggestion {
        padding: 3px 20px;
        font-size: 18px;
        line-height: 24px;
    }

    .tt-suggestion.tt-is-under-cursor { /* UPDATE: newer versions use .tt-suggestion.tt-cursor */
        color: #fff;
        background-color: #0097cf;
    }

    .tt-suggestion p {
        margin: 0;
    }

  </style>
</head>
<body class="index-page">
  <!-- Navbar -->
  <nav id="navbar-main" class="navbar navbar-main navbar-expand-lg bg-white navbar-light position-sticky top-0 shadow py-2">
    <div class="container">
      <a class="navbar-brand mr-lg-5" href="{{ url('/') }}">
        <img src="{{ asset('img/brand/blue.png') }}">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-collapse collapse" id="navbar_global">
        <div class="navbar-collapse-header">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a href="{{ url('/') }}">
                <img src="{{ asset('img/brand/blue.png') }}">
              </a>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
          <li class="nav-item dropdown">
            <a href="https://demos.creative-tim.com/argon-design-system/#" class="nav-link" data-toggle="dropdown" role="button">
              <i class="ni ni-ui-04 d-lg-none"></i>
              <span class="nav-link-inner--text">Components</span>
            </a>
            <div class="dropdown-menu dropdown-menu-xl">
              <div class="dropdown-menu-inner">
                <a href="https://demos.creative-tim.com/argon-design-system/docs/getting-started/overview.html" class="media d-flex align-items-center">
                  <div class="icon icon-shape bg-gradient-primary rounded-circle text-white">
                    <i class="ni ni-spaceship"></i>
                  </div>
                  <div class="media-body ml-3">
                    <h6 class="heading text-primary mb-md-1">Getting started</h6>
                    <p class="description d-none d-md-inline-block mb-0">Learn how to use compiling Scss, change brand colors and more.</p>
                  </div>
                </a>
                <a href="https://demos.creative-tim.com/argon-design-system/docs/foundation/colors.html" class="media d-flex align-items-center">
                  <div class="icon icon-shape bg-gradient-success rounded-circle text-white">
                    <i class="ni ni-palette"></i>
                  </div>
                  <div class="media-body ml-3">
                    <h6 class="heading text-primary mb-md-1">Foundation</h6>
                    <p class="description d-none d-md-inline-block mb-0">Learn more about colors, typography, icons and the grid system we used for .</p>
                  </div>
                </a>
                <a href="https://demos.creative-tim.com/argon-design-system/docs/components/alerts.html" class="media d-flex align-items-center">
                  <div class="icon icon-shape bg-gradient-warning rounded-circle text-white">
                    <i class="ni ni-ui-04"></i>
                  </div>
                  <div class="media-body ml-3">
                    <h5 class="heading text-warning mb-md-1">Components</h5>
                    <p class="description d-none d-md-inline-block mb-0">Browse our 50 beautiful handcrafted components offered in the Free version.</p>
                  </div>
                </a>
              </div>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a href="https://demos.creative-tim.com/argon-design-system/#" class="nav-link" data-toggle="dropdown" role="button">
              <i class="ni ni-collection d-lg-none"></i>
              <span class="nav-link-inner--text">Examples</span>
            </a>
            <div class="dropdown-menu">
              <a href="https://demos.creative-tim.com/argon-design-system/examples/landing.html" class="dropdown-item">Landing</a>
              <a href="https://demos.creative-tim.com/argon-design-system/examples/profile.html" class="dropdown-item">Profile</a>
              <a href="https://demos.creative-tim.com/argon-design-system/examples/login.html" class="dropdown-item">Login</a>
              <a href="https://demos.creative-tim.com/argon-design-system/examples/register.html" class="dropdown-item">Register</a>
            </div>
          </li>
        </ul>
    
      </div>
    </div>
  </nav>
  <!-- End Navbar -->
  <div class="wrapper">
    <div class="section section-hero section-shaped">
      <div class="shape shape-style-1 shape-primary">
        <span class="span-150"></span>
        <span class="span-50"></span>
        <span class="span-50"></span>
        <span class="span-75"></span>
        <span class="span-100"></span>
      </div>
      <div class="page-header">
        <div class="container shape-container d-flex align-items-center py-lg">
          <div class="col px-0">
            <div class="row align-items-center justify-content-center">
              <div class="col-lg-12 text-center">
                <h1 class="card-title text-uppercase text-muted mb-0">Busqueda</h1>
                <!--<img src="{{ asset('img/brand/white.png') }}" style="width: 200px;" class="img-fluid">-->
                <form class="mt-4 mb-3">  <!-- d-md-none Ocultar-->
                  <div>
                    <!--<input type="search" id="search"
                        class="form-control form-control-rounded form-control-prepended" 
                        placeholder="¿Qué trámite deseas realizar?" aria-label="Search" style="width: 100%;">-->
                      <input type="text" placeholder="¿Que producto buscas?" 
                         
                          name="query" id="search" style="width: 100%;">  <!--  class="form-control" -->
                      <button class="btn btn-primary btn-just-icon" type="submit">
                        <i class="material-icons">Busqueda</i>
                      </button>
                  </div>
                </form>
          
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>



    <section class="section section-lg section-shaped">
      <div class="shape shape-style-1 shape-primary">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
      <div class="container py-md">
        <div class="row row-grid justify-content-between align-items-center">
          @foreach($categories as $category) 
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">{{ $category->name }}</h5>
                      <!--<span class="h2 font-weight-bold mb-0">35</span>-->
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                        <i class="fas fa-chart-bar"></i>
                      </div>
                    </div>
                  </div>
                  <!-- <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                    <span class="text-nowrap">Since last mont</span>
                  </p>-->
                </div>
              </div>
            </div>
          @endforeach
           
       
        </div>
      </div>
      <!-- SVG separator -->
   
    </section>

   
    
  </div>
  </div><!-- Typography -->
 
  
 
  
  </div>

  <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
  <!-- Argon JS -->
  <script src="{{ asset('js/argon.js?v=1.0.0') }}"></script>
<!-- 
  <script src="./Argon Design System by Creative Tim_files/bootstrap.min.js.descarga" type="text/javascript"></script>
  <script src="./Argon Design System by Creative Tim_files/perfect-scrollbar.jquery.min.js.descarga"></script> -->
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ 
  <script src="./Argon Design System by Creative Tim_files/bootstrap-switch.js.descarga"></script>-->
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/
  <script src="./Argon Design System by Creative Tim_files/nouislider.min.js.descarga" type="text/javascript"></script>
  <script src="./Argon Design System by Creative Tim_files/moment.min.js.descarga"></script>
  <script src="./Argon Design System by Creative Tim_files/datetimepicker.js.descarga" type="text/javascript"></script>
  <script src="./Argon Design System by Creative Tim_files/bootstrap-datepicker.min.js.descarga"></script> -->
  <!-- Control Center for Argon UI Kit: parallax effects, scripts for the example pages etc -->
 
  <!-- Place this tag in your head or just before your close body tag. 
  <script async="" defer="" src="./Argon Design System by Creative Tim_files/buttons.js.descarga"></script>
  <script src="./Argon Design System by Creative Tim_files/argon-design-system.min.js.descarga" type="text/javascript"></script> 
  <script src="./Argon Design System by Creative Tim_files/jquery.sharrre.js.descarga"></script>  -->

  <script src="{{ asset('/vendor/typeahead/typeahead.bundle.min.js') }}"></script>
  <script>

    /*$(document).ready(function() {
      var branches = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: ['hola', 'prueba1', 'prueba2', 'prueba3']
        //prefetch: '{{ url("/api/branches/json") }}'
      });

      $("#search").typeahead({
      //  hint: true,
     //   highlight: true,
     //   minLength: 1,
    //  },{
        name: 'branches',
        source: branches,   //code_sear
      });


    });*/

    $(document).ready(function() {
      var branches = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        //local: ['hola', 'prueba1', 'prueba2', 'prueba3']
        prefetch: '{{ url("/api/branches/json") }}'
      });

      $("#search").typeahead({
      hint: true,
        highlight: true,
       minLength: 1,
      },{
        name: 'branches',
        source: branches,  
      });


    });

  </script>


