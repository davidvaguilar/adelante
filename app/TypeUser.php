<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeUser extends Model
{
    protected $table = 'type_user';

    public function user(){
        return $this->belongsTo(User::class);
    }


    public function type(){
        return $this->belongsTo(Type::class);
    }

    public function workDays(){
        return $this->hasMany(WorkDay::class, 'type_id');   
      }
}
