<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Auth;
use App\Appointment;
use Carbon\Carbon;

class AppointmentsExport implements FromView
{
    use Exportable;

    /*protected $started_at;

    function __construct($started_at) {
        $this->started_at = $started_at;
    }*/

    public function view(): View
    {
        /*$mes = Carbon::createFromFormat('Y-m-d', $this->started_at)->format('m');
        $anio = Carbon::createFromFormat('Y-m-d', $this->started_at)->format('Y'); */
        
        return view('excels.appointments', [
                'appointments' => Appointment::where('doctor_id', '=', Auth::user()->id)
                        ->get(),
        ]);
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Appointment::all();
    }
}
