<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Appointment;
use App\Type;

class UserController extends Controller
{
    public function edit(){
        $user = auth()->user();
        return view('profile', compact('user'));
    }

    public function update(Request $request){
        $user = auth()->user();
        $user->name = $request->name; 
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->description = $request->description;
        $user->save();

        $notification = 'Los datos han sido actualizados satisfactoriamente.';
        return back()->with(compact('notification'));  //session('notification)
    }

    public function show(User $user)
    {
        $types = $user->types;

      //  $types = Type::where('user_id', $user->id)->get();
        return view('doctors.show', compact('types', 'user'));
    }

    public function chart()
    {
        /*$monthlyCounts = Appointment::select(
                    DB::raw('MONTH(created_at) as month'), 
                    DB::raw('COUNT(1) as count')
                )->groupBy('month')->get()->toArray();

        $counts = array_fill(0, 12, 0);
        foreach( $monthlyCounts as $monthlyCount ){
            $index = $monthlyCount['month']-1;
            $counts[$index] = $monthlyCount['count'];
        }*/
        DB::statement('SET lc_time_names=es_ES;');
       
        $anio = date('Y');
        $mes = date('m');

        $orders = Appointment::select(DB::raw("CONCAT(DAY(scheduled_date),'-',MONTHNAME(scheduled_date)) as mes"),
                            DB::raw('DAY(scheduled_date)'),
                            DB::raw('COUNT(id) as cantidad'))
                    ->whereYear('scheduled_date', $anio)
                    ->whereMonth('scheduled_date', $mes)
                    ->where('status', '=', 'Confirmada')
                    ->where('doctor_id', '=', Auth::user()->id)
                    ->groupBy( DB::raw("CONCAT(DAY(scheduled_date),'-',MONTHNAME(scheduled_date))"), 
                                            DB::raw('DAY(scheduled_date)'))
                    ->orderBy(DB::raw('DAY(scheduled_date)'))        
                    ->get();

        $orders_dates = $orders->pluck('mes');  
        $orders_quantity = $orders->pluck('cantidad'); 

        return view('charts.appointments')->with(['orders_dates' => $orders_dates,
                                                'orders_quantity' => $orders_quantity]);

       // return view('charts.appointments', compact('counts'));

    }
}
