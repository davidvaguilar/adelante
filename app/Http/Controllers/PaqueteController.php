<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\User;
use PDF;

class PaqueteController extends Controller
{
   
    public function qr_qenerate($id){
        $doctor = User::doctors()->findOrFail($id);
        $valor = url('/attention/'.$doctor->id);
   //     $qrcode = base64_encode(QrCode::format('svg')->size(200)->errorCorrection('H')->generate($valor));    // size(500)->   , '../public/qrcodes/qrcode.svg'
      
        $qrcode = base64_encode(QrCode::format('png')
                        ->merge('/public/img/adelante.png', .3, false)
                        ->size(300)->errorCorrection('H')->generate($valor));

        $pdf = PDF::loadView('pdf.qr', ['qrcode'=> $qrcode]);   
     // $url = 'app/pdf/jobs/OT'.$job->code.'-'.$job->branch->code.'-EcorefChile-'.Carbon::now()->format('d-m-Y-H-i-s').'.pdf';
      //$pdf->save($url);
     // return $pdf->download('archivo.pdf');
        return $pdf->stream("CodigoQr-".$doctor->name.".pdf");

    }

    public function index()
    {
        //
    }

   
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
