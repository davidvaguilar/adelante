<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Specialty;
use App\User;

class CategoryController extends Controller
{
  
    public function index(Request $request)
    {
       /* $branch_id = $request->input('branch_id');



        return view('category')->with(['category' => $category]);  */
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $category = Specialty::findOrFail($id);

        
        $branches = User::has('specialties')->get();

        return view('category')->with(['category' => $category, 'branches' => $branches ]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

  
    public function destroy($id)
    {
        //
    }
}
