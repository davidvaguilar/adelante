<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TypeUser;

class WorkDayController extends Controller
{

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $type = TypeUser::findOrFail($id);
        $scheduleDate = old('schedule_date');
        $doctorId = old('doctor_id');
        if( $scheduleDate && $doctorId ){
          $intervals = $scheduleService->getAvailableIntervals($scheduleDate, $doctorId);
        } else {
          $intervals = null;
        }
  
        return view('workday', compact('type', 'intervals'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
