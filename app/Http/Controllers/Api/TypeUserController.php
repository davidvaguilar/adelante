<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TypeUser;

class TypeUserController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Request $request)
    {
       
        $doctor_id = $request->input('doctor_id');
       
        $type_id = $request->input('type_id');
        $typeuser = TypeUser::where('user_id', $doctor_id)->where('type_id', $type_id)->get();

        //dd($typeuser);
        return $typeuser;
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
