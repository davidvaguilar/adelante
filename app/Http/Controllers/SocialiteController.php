<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\User;
use Illuminate\Foundation\Auth\RedirectsUsers;

class SocialiteController extends Controller
{
    use RedirectsUsers;


    private $availableDriver = ['facebook', 'google'];
    public function redirectToProvider($provider){
        if( !in_array($provider, $this->availableDriver) ){
            return redirect()->route('login');
        }
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider){
        if( !in_array($provider, $this->availableDriver) ){
            return redirect()->route('login');
        }

        $userSocialite = Socialite::driver($provider)->user();
    
        if( $userSocialite->getEmail() ){
            $user = User::where('email', $userSocialite->getEmail())->first();
        } else {
            $user = User::where($provider.'_id', $userSocialite->getId())->first();
        }
        
        if( $user ){
            $user->update([
                'name' => $userSocialite->getName(),
                $provider.'_id' => $userSocialite->getId(),
                'avatar' => $userSocialite->getAvatar(),
                'nickname' => $userSocialite->getNickname(),
            ]);
        } else { 
            $user = User::create([
                'name' => $userSocialite->getName(),
                'email' => $userSocialite->getEmail(),
                'password' => '',
                $provider.'_id' => $userSocialite->getId(),
                'avatar' => $userSocialite->getAvatar(),
                'nickname' => $userSocialite->getNickname(),
            ]);
        } 
       // dd($this->redirectPath());
        auth()->login($user);
   
        return redirect()->intended($this->redirectPath());
        //return redirect()->route('home');  // esto esta mal
    }


    public function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
    }

}
