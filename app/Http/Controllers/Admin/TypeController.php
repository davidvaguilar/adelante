<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Type;

class TypeController extends Controller
{
    public function index()
    {
        $types = Type::all();
        return view('types.index', compact('types'));
    }

    public function create()
    {
        return view('types.create');
    }

    private function performValidation(Request $request){
        $rules = [
          'name' => 'required|min:3'
        ];
        $messages = [
          'name.required' => 'Es necesario ingresar un nombre.',
          'name.min' => 'Como minimo el nombre debe tener 3 caracteres.',
        ];
        $this->validate($request, $rules, $messages);
    }

    public function store(Request $request)
    {
        $this->performValidation($request);
        //dd($request->all());
        $type = new Type();
        $type->name = $request->input('name');
        $type->description = $request->input('description');
        $type->save();
        //return back();
        $notification = 'El tipo de atencion se ha registrado correctamente.';
        return redirect('/types')->with(compact('notification'));
    }

    public function show($id)
    {
        //
    }

    public function edit(Type $type)
    {
        return view('types.edit', compact('type'));
    }

    public function update(Request $request, Type $type)
    {
        $this->performValidation($request);

        $type->name = $request->input('name');
        $type->description = $request->input('description');
        $type->save();  //UPDATE
        //return back();
        $notification = 'El tipo de atencion se ha actualizado correctamente.';
        return redirect('/types')->with(compact('notification'));
    }

    public function destroy(Type $type)
    {
        $deletedName = $type->name;
        $type->delete();
        $notification = 'La atencion '.$deletedName.' se ha eliminado correctamente.';
        return redirect('/types')->with(compact('notification'));
    }
}
