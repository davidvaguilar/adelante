<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use App\Specialty;
use App\Type;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class DoctorController extends Controller
{
    public function index()
    {
        //$doctors = User::where('role', 'doctor')->get();
        $doctors = User::doctors()->get();
        return view('doctors.index', compact('doctors'));
    }

    public function create()
    {
        $specialties = Specialty::all();
        $types = Type::all(); 
        return view('doctors.create', compact('specialties', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //dd($request->all());
      $rules = [
          'name' => 'required|min:3',
          'email' => 'required|email',
          'dni' => 'nullable',
          'address' => 'nullable|min:5',
          'phone' => 'nullable|min:6',
          'file' => 'nullable|max:1000|mimes:jpeg,png',   //'machine_id' => 'required'
      ];
// |digits:8
     
      $this->validate($request, $rules);
 
      $user = User::create(
        $request->only('name', 'email', 'dni', 'address', 'phone')
        + [
            'role' => 'doctor',
            'password' => bcrypt($request->input('password'))
        ]
      );

      if( isset($request->file) ){
        $file = $request->file('file');
        $photo_name = 'enterprise'.$user->id.'-'.Carbon::now()->format('dmYHis').'.'.$file->getClientOriginalExtension();
        $photo_extension = $file->getClientOriginalExtension();

        $path = $request->file('file')->storePubliclyAs(
            "img/enterprise", $photo_name
        );

        $user->url = 'app/'.$path;
        $user->save();
      }

      $user->specialties()->attach($request->input('specialties'));
      $notification = 'Empresa se ha registrado correctamente.';
      return redirect('/doctors')->with(compact('notification'));
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $doctor = User::doctors()->findOrFail($id);
        $specialties = Specialty::all();
        $types = Type::all(); 
        $specialty_ids = $doctor->specialties()->pluck('specialties.id');
        $types_ids = $doctor->types()->pluck('types.id');



        return view('doctors.edit', compact('doctor', 'types', 'specialties', 'specialty_ids', 'types_ids'));
    }

    public function update(Request $request, $id)
    {
      $rules = [
          'name' => 'required|min:3',
          'email' => 'required|email',
          'dni' => 'nullable',
          'address' => 'nullable|min:5',
          'phone' => 'nullable|min:6'
      ];
      // |digits:8
      $this->validate($request, $rules);

      $user = User::doctors()->findOrFail($id);

      $data = $request->only('name', 'email', 'dni', 'address', 'phone');
      $password = $request->input('password');
      if( $password ){
          $data['password'] = bcrypt($password);
      }
      $user->fill($data);
      $user->save();

      if( isset($request->file) ){
        $file = $request->file('file');
        $photo_name = 'enterprise'.$user->id.'-'.Carbon::now()->format('dmYHis').'.'.$file->getClientOriginalExtension();
        $photo_extension = $file->getClientOriginalExtension();

        $path = $request->file('file')->storePubliclyAs(
            "img/enterprise", $photo_name
        );    
        $user->url = 'app/'.$path;
        $user->save();
      }

      $user->specialties()->sync($request->input('specialties'));
      $user->types()->sync($request->input('types'));

      $notification = 'La informacion se ha actualizado correctamente.';
      return redirect('/doctors')->with(compact('notification'));
    }

    public function destroy($id)
    {
        $user = User::doctors()->findOrFail($id);
        $doctorName = $user->name;
        $user->delete();
        $notification = "La empresa $doctorName se ha eliminado correctamente.";
        return redirect('/doctors')->with(compact('notification'));
    }

   
}
