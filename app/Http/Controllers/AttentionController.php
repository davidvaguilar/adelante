<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AttentionController extends Controller
{

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }


    public function show(User $user)
    {
        //$types = $user->types;   // 

        $types = TypeUser::where('user_id', $user->id)->get();

      //  $types = Type::where('user_id', $user->id)->get();
        return view('type', compact('types', 'user'));
    }

  
    public function edit($id)
    {
        //
    }

 
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
