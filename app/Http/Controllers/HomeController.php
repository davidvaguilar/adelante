<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Specialty;
use App\User;

class HomeController extends Controller
{
    public function index()
    {

        $categories = Specialty::has('users')->get();

        $branches = User::has('specialties')->get();
       /* dd($categories);
        $branches = $categories->users;*/

        return view('welcome')->with(['categories' => $categories, 'branches' => $branches]);
    }
}
