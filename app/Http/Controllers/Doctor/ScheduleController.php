<?php

namespace App\Http\Controllers\Doctor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WorkDay;
use Carbon\Carbon;
use  App\TypeUser;

class ScheduleController extends Controller
{
    private $days = [
        'Lunes', 'Martes', 'Miercoles',
        'Jueves', 'Viernes', 'Sábado', 'Domingo'
    ];

    public function edit(Request $request){
      
      $type_id = $request->input('type_id');

      $type_user = TypeUser::where('type_id', $type_id)->where('user_id', $request->input('user_id') )->firstOrFail();
      $type_user->interval = $request->input('interval');
      $type_user->save();
     
      $workDays = WorkDay::where('type_id', $type_id)->where('user_id', auth()->id() )->get();

      if( count($workDays) > 0 ){
        $workDays->map( function($workDay ){
         /* $workDay->morning_start = (new Carbon($workDay->morning_start))->format('g:i A');
          $workDay->morning_end = (new Carbon($workDay->morning_end))->format('g:i A');
          $workDay->afternoon_start = (new Carbon($workDay->afternoon_start))->format('g:i A');
          $workDay->afternoon_end = (new Carbon($workDay->afternoon_end))->format('g:i A');*/
          $workDay->morning_start = (new Carbon($workDay->morning_start))->format('H:i');
          $workDay->morning_end = (new Carbon($workDay->morning_end))->format('H:i');
          $workDay->afternoon_start = (new Carbon($workDay->afternoon_start))->format('H:i');
          $workDay->afternoon_end = (new Carbon($workDay->afternoon_end))->format('H:i');
          return $workDay;
        });
      } else {
        $workDays = collect();
        for ($i=0; $i < 7 ; ++$i) {
          $workDays->push(new WorkDay());
        }
      }

      $days = $this->days;
      return view('schedule', compact('workDays', 'days', 'type_user'));
    }

    public function store(Request $request)
    {
      //dd($request->all());
      
      $active = $request->input('active') ?: [];
      $morning_start = $request->input('morning_start');
      $morning_end = $request->input('morning_end');
      $afternoon_start = $request->input('afternoon_start');
      $afternoon_end = $request->input('afternoon_end');

      $errors = [];
      for( $i=0; $i<7; ++$i ){
        if( $morning_start[$i] > $morning_end[$i] ){
          $errors []= 'Las horas del turno mañana son inconsistente para el dia '. $this->days[$i]. '.';
        }
        if( $afternoon_start[$i] > $afternoon_end[$i] ){
          $errors []= 'Las horas del turno tarde son inconsistente para el dia '. $this->days[$i]. '.';
        }

        WorkDay::updateOrCreate([
            'day' => $i,
            'user_id' => auth()->id(),
            'type_id' => $request->input('type_id'),
          ],
          [
            'active' => in_array($i, $active),
            'morning_start' => $morning_start[$i],
            'morning_end' => $morning_end[$i],
            'afternoon_start' => $afternoon_start[$i],
            'afternoon_end' => $afternoon_end[$i]
        ]);
      }
      if( count($errors) > 0){
        return back()->with(compact('errors'));
      }
      $notification = 'Los cambios se han guardado correctamente.';
      return back()->with(compact('notification'));
    }
}
