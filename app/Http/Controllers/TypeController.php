<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TypeUser;
use App\User;

class TypeController extends Controller
{

    public function index(Request $request)
    {
        $branch_id = $request->input('branch_id');
        $type = TypeUser::where('user_id', $branch_id)->first();
        $typeusers = TypeUser::where('user_id', $branch_id)->get();
        $branches = User::has('specialties')->get();

        return view('type')->with(['typeusers' => $typeusers, 'type'=>$type,  'branches' => $branches]);  
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $type = TypeUser::where('user_id', $id)->first();
        $typeusers = TypeUser::where('user_id', $id)->get();
        $branches = User::has('specialties')->get();
        return view('type')->with(['typeusers' => $typeusers, 'type'=>$type, 'branches' => $branches]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
