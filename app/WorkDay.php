<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkDay extends Model
{
    protected $fillable = [
      'day', 'active','morning_start','morning_end', 'afternoon_start', 'afternoon_end', 'type_id', 'user_id'
    ];

    public function type(){
      return $this->belongsTo(Type::class);
    }
}
