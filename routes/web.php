<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');    // return redirect('/login'); //
});*/
/*
Route::get('/politicas', function () {
    return view("app/politicas.pdf");;    // return redirect('/login'); //
});*/

Route::get('/test', function () {
  return view('test');
});

Route::get('/', 'HomeController@index'); 
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
Route::get('/categories/{category}', 'CategoryController@show'); 

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');    //{{ route('home') }}
Route::get('/login/{provider}', 'SocialiteController@redirectToProvider');
Route::get('/login/{provider}/callback', 'SocialiteController@handleProviderCallback');

Route::middleware('auth')->group(function() {

  Route::get('/chart', 'UserController@chart');

  

  Route::get('/qr_qenerate/{user}', 'PaqueteController@qr_qenerate');
  Route::get('/attention', 'TypeController@index'); 

  Route::get('/attention/{user}', 'TypeController@show'); 
  Route::get('/workday/{type}', 'WorkDayController@show'); 

  Route::get('/profile', 'UserController@edit');
  Route::post('/profile', 'UserController@update');  

  Route::get('/users/{user}', 'UserController@show');

  Route::get('/appointments', 'AppointmentController@index');
  Route::get('/appointments/create', 'AppointmentController@create');

  Route::get('/appointments/{appointment}', 'AppointmentController@show');  //ver cita
  Route::post('/appointments', 'AppointmentController@store');  //procesar formulario 

  Route::get('/appointments/{appointment}/cancel', 'AppointmentController@showCancelForm');
  Route::post('/appointments/{appointment}/cancel', 'AppointmentController@postCancel');
  Route::post('/appointments/{appointment}/confirm', 'AppointmentController@postConfirm');
  
  Route::post('/appointments/{appointment}/attended', 'AppointmentController@postAttended');
});

Route::middleware(['auth', 'admin'])->namespace('Admin')->group(function() {

 
  //Type
  Route::get('/types', 'TypeController@index');
  Route::get('/types/create', 'TypeController@create');  
  Route::get('/types/{type}/edit', 'TypeController@edit');
  Route::post('/types', 'TypeController@store'); //envio form
  Route::put('/types/{type}', 'TypeController@update');
  Route::delete('/types/{type}', 'TypeController@destroy');

  //Specialty
  Route::get('/specialties', 'SpecialtyController@index');
  Route::get('/specialties/create', 'SpecialtyController@create'); //form registro
  Route::get('/specialties/{specialty}/edit', 'SpecialtyController@edit');
  Route::post('/specialties', 'SpecialtyController@store'); //envio form
  Route::put('/specialties/{specialty}', 'SpecialtyController@update');
  Route::delete('/specialties/{specialty}', 'SpecialtyController@destroy');
  //doctors
  Route::resource('/doctors', 'DoctorController');
  //pacients
  Route::resource('/patients', 'PatientController');

  Route::get('/charts/appointments/line', 'ChartController@appointments');
  Route::get('/charts/doctors/column', 'ChartController@doctors');
  Route::get('/charts/doctors/column/data', 'ChartController@doctorsJson');

});

Route::middleware(['auth', 'doctor'])->namespace('Doctor')->group(function() {

  Route::get('/excel/appointment', 'ExcelController@exportAppointment')->name('admin.appointments.excel');
 
  Route::get('/schedule', 'ScheduleController@edit');
  Route::post('/schedule', 'ScheduleController@store');
});

