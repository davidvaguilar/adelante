<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      User::create([
        'name' => 'Administrador',
        'email' => 'david.villegas.aguilar@gmail.com',
        'password' => bcrypt('123123'),
        'role'=> 'admin'
      ]);
      User::create([
        'name' => 'Administrador Hans',
        'email' => 'hans.stevens@adlinks.cl',
        'password' => bcrypt('123123'),
        'role'=> 'admin'
      ]);

     /* User::create([
        'name' => 'Paciente Test',
        'email' => 'paciente@correo.com',
        'password' => bcrypt('123123'),
        'role'=> 'patient'
      ]);*/

      User::create([
        'name' => 'Empresa Prueba 1',
        'email' => 'empresa1@adlinks.cl',
        'password' => bcrypt('123123'),
        'role'=> 'doctor'
      ]);

      User::create([
        'name' => 'Empresa Prueba 2',
        'email' => 'empresa2@adlinks.cl',
        'password' => bcrypt('123123'),
        'role'=> 'doctor'
      ]);

      User::create([
        'name' => 'Empresa Prueba 3',
        'email' => 'empresa3@adlinks.cl',
        'password' => bcrypt('123123'),
        'role'=> 'doctor'
      ]);

      //factory(User::class, 1)->states('patient')->create();
    }
}
